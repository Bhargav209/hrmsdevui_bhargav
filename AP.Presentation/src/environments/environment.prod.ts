export const environment = {
  production: false, // set to true
  ServerUrl: 'http://hrmsdev/HRMS2Dev.Service/api', // prod url
  base64Key: '16rdKQfqN3L4TY7YktgxBw==',
  CryptoKey: '8080808080808080',
  logError: '/ErrorLog/LogError', 
  CryptoIv: '8080808080808080',
  CLIENT_ID: "b98d2445-204a-4dee-a2a5-dc48818e0d77",  // set HRMS Azure id's
  TENANT_ID: "9b0861df-ad09-47df-b4d4-9a00828ab9f0",  // set HRMS Azure id's
  GRAPH_RESOURCE: "https://graph.microsoft.com",
  RedirectURL : "", //production redirect url,
  Image : "https://graph.microsoft.com/v1.0/me/photo/$value",
  LogOutURL :"https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=http://localhost:4200", // change localhost url ...
};
