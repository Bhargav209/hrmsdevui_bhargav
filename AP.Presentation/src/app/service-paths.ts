export const API = {
  'Menu': {
    'GetMenuDetails': '/Menu/GetMenuDetails?roleName='
  },

  'logError': '/ErrorLog/LogError',
  
  'PagingConfigValue': {
    'PageSize': 10,
    'PageDropDown': [
      10,
      15,
      20
    ]
  },

  'Roles': {
    'list': '/Role/GetRoles',
    'createRole': '/Role/CreateRole',
    'updateRole': '/Role/UpdateRole',
    'createRoleSkills': '/Role/AddSkillsToRole',
    'updateRoleskills': '/Role/UpdateRoleSkills',
    'getRolebyID': '/Role/GetRoleByRoleID?roleID=',
    'getRolesByDept': '/RoleMaster/GetRoleMasterDetails?departmentId=',
    'getRoleSuffixAndPrefix': '/Role/getRoleSuffixAndPrefix?departmentId=',
    'GetRoleDetailsByDepartmentID': '/Role/GetRoleDetailsByDepartmentID?DepartmentId=',
    'GetLoggedInUserRoles': '/KRA/GetLoggedInUserRoles?employeeID=',

  },
  'WorkStation': {      
    'GetWorkStationListByBayId': '/WorkStation/GetWorkStationListByBayId?BayIds=',
    'GetWorkStationDataCount': '/WorkStation/WorkStationReportCount?bayId=',
    'GetWorkStationDetailByWorkStationCode':'/WorkStation/GetWorkStationDetailByWorkStationCode?workstationCode=',
    'DeskAllocation':'/WorkStation/EmployeeDeskAllocation?employeeId=',
    'ReleaseDesk':'/WorkStation/ReleaseDesk?employeeId=',
    'GetBaysList': '/Bay/GetBaysList',
    'create': '/Bay/CreateWorkStation'
  },
  'notificationemail': {
    'GetNotificationType': '/SystemNotification/GetNotificationType',
    'save': '/SystemNotification/SaveNotificationConfig',
    'list': '/SystemNotification/GetNotificationConfiguration?notificationType=',
    'GetAllEmailIds': '/SystemNotification/GetAllEmailIds',
  },
  'NotificationConfiguration': {
    'GetNotificationCofigurationByNotificationType': '/NotificationConfiguration/GetNotificationConfigurationDetailsByNotificationTypeId?notificationTypeID=',
    'SaveNotificationCofiguration': '/NotificationConfiguration/CreateNotificationConfiguration',
    'UpdateNotificationCofiguration': '/NotificationConfiguration/UpdateNotificationConfiguration',
    'FromEmail': 'it@senecaglobal.com',
    'GetNotificationType':  '/NotificationType/GetNotificationType'
  },
  'NotificationType': {
    'GetNotificationType': '/NotificationType/GetNotificationType',
    'AddNotificationType': '/NotificationType/CreateNotificationType',
    'UpdateNotificationType': '/NotificationType/UpdateNotificationType',
    'DeleteNotificationType': '/NotificationType/DeleteNotificationTypeByTypeId?notificationTypeId='
  },
  'common': {
    'getGetBusinessValues': '/Common/GetBusinessValues',
    'logError': '/ErrorLog/LogError'
  },
  'EmployeeStatus': {
    'GetUsers': '/UserRole/GetUnMappedUsers',
    'GetNames': '/UserRole/GetNames',
    'UpdateEmployeeStatus': '/UserRole/UpdateEmployeeStatus',
    'GetResignStatus': '/UserRole/GetEmployeeResignStatus',
    'MapAssociateId': '/UserRole/MapAssociateId'
  },
  'AssignMenusRole': {
    'getSourceMenuRoles': '/Menu/GetSourceMenuRoles?RoleId=',
    'getTargetMenuRoles': '/Menu/GetTargetMenuRoles?RoleId=',
    'addTargetMenuRoles': '/Menu/AddTargetMenuRoles'
  },
  'Masterdata': {
    'getDepartmentsList': '/UserDepartment/GetUserDepartmentDetails',
    'getRoles': '/UserRole/GetRoles',
    'getRolesByDepartmentID': '/Role/GetRolesByDepartmentID?DepartmentId=',
    'getRolesByProjectId': '/ProjectRoleAssignment/GetRolesByProjectId?projectId=',
    'getCompetencyAreas': '/CompetencyArea/GetCompetencyAreaDetails',
    'getSkillGroupsByCompetenctArea': '/SkillGroup/GetSkillGroupByCompetencyAreaID?competencyAreaID=',
    'getSkillsBySkillGroup': '/Common/GetSkillsbySkillGroupID?SkillGroupID=',
    'getProficiencyLevels': '/ProficiencyLevel/GetProficiencyLevels',
    'getProjectList': '/MasterData/GetProjectsList',
    'getManagersAndCompetencyLeads': '/MasterData/GetManagersAndCompetencyLeads',
    'getDomains': '/Domain/GetDomains',
    'getFinancialYears': '/MasterData/GetFinancialYearList',
    'getEmployeeNameByEmployeeId': '/MasterData/GetEmployeeNameByEmployeeId?employeeID=',
    'getAllEmailIDs': '/NotificationConfiguration/GetEmployeeWorkEmails?searchString=',
    'getEmployeesAndManagers': '/Project/GetEmployees?searchString=',
    'getAllLeadsManagers': '/Project/GetAllLeadsManagers?searchString=',
    'getAllAssociateList': '/Appreciation/GetAssociateNamesList',
    'getMasterSkillList': '/AssociateSkills/GetSkillsData',
    'getPractiseAreas': '/PracticeArea/GetPracticeAreas',
    'getGradesDetails': '/UserGrade/GetGradesDetails',
    'getClientList': '/Client/GetClientsDetails',
    'getDesignationList': '/Designation/GetDesignationDetails',
    'getAllocationPercentages': '/Common/GetAllocationPercentages',
    'getProgramManagers': '/MasterData/GetProgramManagers',
    'getProjectTypes': '/Project/GetProjectTypes',
    'getRoleCategory': '/KRA/GetRoleCategory',
    'getKRAOperators': '/KRA/GetKRAOperators',
    'getKRAMeasurementType': '/KRA/GetKRAMeasurementType',
    'getKRATargetPeriods': '/KRA/GetKRATargetPeriods',
    'getKRAScales': '/KRA/GetKRAScales',
    'getKRAScaleValues': '/KRA/GetKRAScaleValues',
    'getDepartmentByDepartmentTypeId': '/MasterData/GetDepartmentByDepartmentTypeId?departmentTypeId=',
    'getDepartmentTypes': '/MasterData/GetDepartmentTypes',
    'getDesignationByString': '/MasterData/GetDesignations?searchString=',
    'getGradeByDesignation': '/ProspectiveAssociate/GetGradeByDesignation?designationId=',
    'GetCategories': '/Category/GetsCategory',
    'getAllDepartments': '/UserDepartment/GetDepartmentDetails',
    'getKRARoles':'/KRARole/GetKRARoles'
  },
  'Technology': {
    'list': '/PracticeArea/GetPracticeAreas'
  },
  'KraDetailsForDepartmentHead': {
      "getDepartmentsByEmployeeId": "/KRA/GetDepartmentsByDepartmentHeadID?employeeID=",
      "getKraRolesForDH": "/KRA/GetKRARolesForDH?financialYearID=",
      "sendBackToHRHead": "/KRA/SendBackForHRHeadReview",
      "createKraByDH": "/KRA/CreateKRAByDepartmentHead",
      "updateKraByDH": "/KRA/UpdateKRARoleMetricByDepartmentHead",
      "getKraWorkFlowPendingWithEmployee": "/KRA/GetKRAWorkFlowPendingWithEmployeeID?financialYearID="
    },

  'CompetencyArea': {
    'list': '/CompetencyArea/GetCompetencyAreaDetails/',
    'create': '/CompetencyArea/CreateCompetencyArea/',
    'update': '/CompetencyArea/UpdateCompetencyArea/',
  },
  'Grades': {
    'list': '/UserGrade/GetGradesDetails',
    'create': '/UserGrade/CreateGrade/',
    'update': '/UserGrade/UpdateGrade/',

  },
  'ProjectType': {
    'list': '/ProjectTypeMaster/GetProjectTypes/',
    'create': '/ProjectTypeMaster/CreateProjectType/',
    'update': '/ProjectTypeMaster/UpdateProjectType/',
  },
  'ProficiencyLevel': {
    'list': '/ProficiencyLevel/GetProficiencyLevels',
    'create': '/ProficiencyLevel/CreateProficiencyLevel/',
    'update': '/ProficiencyLevel/UpdateProficiencyLevel',
  },
  'ClientBillingRole': {
    'list': '/ClientBillingRole/GetClientBillingRoles',
    'create': '/ClientBillingRole/CreateClientBillingRole',
    'update': '/ClientBillingRole/UpdateClientBillingRole',
    'delete': '/ClientBillingRole/DeleteClientBillingRole?clientBillingRoleId=',
    'getClientBillingRolesByProjectId': '/ClientBillingRole/GetClientBillingRolesByProjectId?projectId=',
    'CloseClientBillingRole' : '/ClientBillingRole/CloseClientBillingRole?clientBillingRoleId=',
    'GetEmployeesByClientBillingRoleId': '/ClientBillingRole/GetEmployeesByClientBillingRoleId?projectId=',
  },
  'Designation': {
    'list': '/Designation/GetDesignationDetails',
    'create': '/Designation/SaveDesignationDetails',
    'update': '/Designation/UpdateDesignationDetails'

  },
  'Department': {
    'list': '/UserDepartment/GetDepartmentDetails',
    'create': '/UserDepartment/CreateDepartment',
    'update': '/UserDepartment/UpdateUserDepartmentDetails',
    'GetRoleDetailsByDepartmentID': '/Role/GetRoleDetailsByDepartmentID?DepartmentId=',
    'GetDepartmentById': '/MasterData/GetDepartmentByDepartmentTypeId?departmentTypeId='
  },

  'UserNames': {
    'list': '/UserRole/GetUsers',
    'getById': '/UserRole/GetUserRoleDetailsbyUserID?userID=',
    'getRoleByDept': '/RoleMaster/GetRoleMasterDetails?departmentId=',
    'update': '/UserRole/UpdateUserRole',
    'create': '/UserRole/AssignRole'
  },
  
  "KRA": {
    "getFinancialYears": "/MasterData/GetFinancialYearList",
    "getKRAGroupsByYear": "/KRA/GetKRAGroups?financialYearId=",
    "getKRAGroupsByFinancialYear": "/KRA/GetKRAGroupsByFinancial?financialYearId=",
    "createKRAGroup": "/KRA/CreateKRAGroup",
    "createKRADefinition": "/KRA/CreateKRADefinition",
    "getKRADefinitionsById": "/KRA/GetKRADefinitionById?kraGroupId=",
    "updateKRADefinition": "/KRA/UpdateKRADefinition",
    "deleteKRADefinition": "/KRA/DeleteKRADefinition",
    "deleteKRAMetricByKRADefinitionId": "/KRA/DeleteKRAMetricByKRADefinitionId",
    "sendForDepartmentApproval": "/KRA/SubmitKRAForApproval",
    "approveKRA": "/KRA/ApproveKRA",
    "SendBackForHRMReview": "/KRA/SendBackForHRMReview",
    "SendBacktoDepartmentHead": "/KRA/SendBacktoDepartmentHead",
    "SendtoHRHeadReview": "/KRA/SendtoHRHeadReview",
    "getKRAComments": "/KRA/GetKRAComments?financialYearId=",
    "getRoles": "/Role/GetRoleSuffixAndPrefix",
    "getRolebyID": "/Role/GetRoleByRoleID?roleID=",
    "CloneKras": "/KRA/CloneKRAs",
    "getCurrentFinancialYear": "/MasterData/GetCurrentFinancialYear",
    "createKRAForRole": "/KRARole/CreateKRAForRole",
    "deleteKraRoleByYearIdAndRoleId": "/KRA/DeleteKRARoleByFinancialYearAndRoleId?roleID=",
    "submitKraForHRHeadReview": "/KRA/SubmitKRAForHRHeadReview",
    "getKRARolesByDepartmentID " : "/KRA/GetKRARolesByDepartmentID?departmentID=",
    "getKraRolesByDepartmentId": "/KRA/GetKRARolesForReview?financialYearID=",
    "getKraAspects": "/KRAAspects/GetKRAAspect?departmentId=",
    "getRoleName": "/KRARole/GetRoleName?roleId=",
    "getDepartmentName": "/KRARole/GetDepartmentName?departmentId=",
    "getFinancialYear": "/KRARole/GetFinancialYear?financialYearId=",
    "cloneYearwise": "/KRA/CloneKRATemplateForFinancialYear",
    "getKRAGroupList": "/Role/GetKRATitleByDepartmentID?departmentID=",
    "addKRAComments": "/KRA/AddKRAComments",
    "deleteKRAGroup": "/KRA/DeleteKRAGroup",
  },

  'FinancialYears': {
    "GetFinancialYearList": "/FinancialYear/GetFinancialYears",
    "GetCurrentFinancialYear": "/FinancialYear/GetCurrentFinancialYearByMonth",
    "CreateFinancialYear": "/FinancialYear/CreateFinancialYear",
    "UpdateFinancialYear": "/FinancialYear/UpdateFinancialYear",
  },

  "CustomKras": {
    "getProjectsByProgramManagerId": "/CustomKRAs/GetProjectsByProjectManagerID?projectManagerID=",
    "getEmployeesByProjectId": "/CustomKRAs/GetEmployeesByProjectID?projectID=",
    "getEmployeesByDepartment": "/CustomKRAs/GetEmployeesForDepartment?employeeID=",
    "saveCustomKras": "/CustomKRAs/SaveCustomKRAs",
    "editCustomKras": "/CustomKRAs/EditCustomKRAs",
    "deleteCustomKra": "/CustomKRAs/DeleteCustomKRA?customKRAID=",
    "getOrganizationAndCustomKrasOfEmployee": "/CustomKRAs/GetOrganizationAndCustomKRAsForPMDH?employeeID=",
  },
  
  'KraAspect': {
    'getKraAspects': '/KRAAspects/GetKRAAspect?departmentId=',
    'creatKraAspect': '/KRAAspects/CreateKRAAspect',
    'updateKraAspect': '/KRAAspects/UpdateKRAAspect',
    'deleteKRAAspect' : '/KRAASpects/DeleteKRAAspect',

  },
  "KraMeasurementType": {
    "deleteKraMeasurementType": "/KRA/DeleteKRAMeasurementType?KRAMeasurementTypeId=",
    "createKraMeasurementType": "/KRA/CreateKRAMeasurementType",
    "updateKraMeasurementType": "/KRA/UpdateKRAMeasurementType"
  },
  "AspectMaster": {
    "GetAspectMasterList": "/AspectsMaster/GetAspectsMaster",
    "CreateAspect": "/AspectsMaster/CreateAspectMaster",
    "UpdateAspect": "/AspectsMaster/UpdateAspectMaster",
    "DeleteAspect": "/AspectsMaster/DeleteAspectMaster?aspectId="
  },
  "KRAScaleMaster": {
    "getKRAScale": "/KRAScaleMaster/GetKRAScaleMasters",
    "getKRADescriptionDetails":"/KRAScaleMaster/GetKRAScaleDetailsByMaster?scaleMasterId=",
    "createKRAScale": "/KRAScaleMaster/CreateScaleMaster",
    "updateKRAScale": "/KRAScaleMaster/UpdateScaleDetails?MaximumScale=",
    "deleteKRAScale": "/KRAScaleMaster/DeleteScaleMaster?kraScaleMasterId="
  },
  "AssociateKras": {
    "GetAssociateKRAs": "/KRA/GetViewKRAByEmployee?employeeID=",
    "getEmployeesByProjectID": "/MasterData/GetEmployeesForViewKraInformation?departmentId=",
    "GenerateKRAPdfForAllAssociates" : "/AssociateKRAMapping/GenerateKRAPdfForAllAssociates?overideExisting=",
  },
  "MapAssociateRole": {
    "getEmployeesByProjectID": "/MasterData/GetEmployeesByDepartmentAndProjectID?departmentId=",
    "getRolesOfDeliveryDepartment": "/MasterData/GetKRARolesForProjectDeliveryDepartment",
    "mapRole": "/AssociateKRAMapping/UpdateAssociateRole",
    "getRolesbyDepartmentId": "/Role/GetRolesByDepartmentID?DepartmentId=",
    "getKraRolesByDepartmentId" : "/Role/GetKRARolesByDepartmentID?departmentID=",
    "getEmployeesByKraRoleIdAndFinancialYearId" : "/KRARole/GetEmployeesByKRARole?KRARoleId="
  },

  'Clients': {
    'list': '/Client/GetClientsDetails/',
    'create': '/Client/CreateClient/',
    'update': '/Client/UpdateClient/'
  },

  'PracticeArea': {
    'list': '/PracticeArea/GetPracticeAreas',
    'create': '/PracticeArea/AddPracticeArea/',
    'update': '/PracticeArea/UpdatePracticeArea/'
  },

  'Domain': {
    'list': '/Domain/GetDomains',
    'create': '/Domain/CreateDomain',
    'update': '/Domain/UpdateDomain/'
  },

  'SkillGroup': {
    'list': '/SkillGroup/GetsSkillGroup',
    'create': '/SkillGroup/CreateSkillGroup/',
    'update': '/SkillGroup/UpdateSkillGroup/',
    'getSkillGroupsByCompetenctArea': '/SkillGroup/GetSkillGroupByCompetencyAreaID?competencyAreaID='
  },

  'Skills': {

    'list': '/AssociateSkills/GetSkillsData',
    'create': '/AssociateSkills/CreateSkillsMaster/',
    'update': '/AssociateSkills/UpdateSkillMasterDetails/'
  },

  'projects': {
    'getProjectbyID': '/Project/GetProject?projectId=',
    'getProjectTypes': '/Project/GetProjectTypes',
    'getEmpList': '/Project/GetEmpList',
    'getClients': '/Project/GetCustomers',
    'getStatusDetails': '/StatusMaster/GetStatusMasterDetails',
    'addProject': '/Project/PostProject',
    'updateProject': '/Project/PutProject',
    'getProgrammangers': '/Project/GetManagerList',
    'getProjectList': '/Project/GetProjectsList?userRole=',
    'getManagers': '/Project/AssignProgramManagerToProject?userRole=',
    'deleteProject': '/Project/DeleteProjectDetails?projectId=',
    'submitForApproval' : '/Project/SubmitForApproval?projectId=',
    'approveOrRejectByDH' : '/Project/ApproveOrRejectByDH?projectId=',
    'GetProjectsStatuses' : '/Project/GetProjectsStatuses',
    'canCloseProject' :'/Project/HasActiveClientBillingRoles',
    'closeProject' : '/Project/CloseProject',
  },
  'sow': {
    'getSOWDetailsById': '/Project/GetSowsByProjectId?projectId=',
    'createSOW': '/Project/PostSOW',
    'GetAddendumsBySOWId': '/project/GetAddendums?Id=',
    'GetSowDetails': '/project/GetSowDetails?id=',
    'delete': '/project/DeleteSOW?Id=',
    'GetSddendumDetailsById': '/project/GetAddendumDetailsById?id=',
    'UpdateSOWAndAddendumDetails': '/project/UpdateSOWAndAddendumDetails'
  },
  'SkillSearch': {
    'getEmployeeDetailsBySkill': '/SkillSearch/getEmployeeDetailsBySkill/',
    'getAllSkillDetails': '/SkillSearch/GetAllSkillDetails?empId=',
    'GetSkillsBySearchString': '/SkillSearch/GetSkillsBySearchString?searchString=',
    'GetProjectDetailByEmployeeId' : '/SkillSearch/GetProjectDetailByEmployeeId?employeeId='
  },
  'AssociateJoining': {
    'getJoinedAssociates': '/ProspectiveAssociate/GetJoinedAssociates'
  },
  'AssociateInformation': {
    'getInfoAssociates': '/AssociatePersonalDetail/GetAssociateDetails'
  },
  'personal': {
    'getByEmpId': '/AssociatePersonalDetail/GetPersonalDetailsByID?empID=',
    'getByPAId': '/ProspectiveAssociate/GetPADetailsByID?ID=',
    'save': '/AssociatePersonalDetail/AddPersonalDetails',
    'update': '/AssociatePersonalDetail/UpdatePersonalDetails',
    'getManagersAndLeads': '/Project/GetManagersAndLeads',
    'getUserDepartmentDetails': '/UserDepartment/GetUserDepartmentDetails',
    'getDesignations': '/ProspectiveAssociate/GetDesignationByGrade?gradeID=',
    'getGradesDetails': '/UserGrade/GetGradesDetails',
    'getHRAdvisors': '/ProspectiveAssociate/GetHRAdvisors',
    'getEmpTypes': '/ProspectiveAssociate/GetEmpTypes',
    'getTechnologies': '/PracticeArea/GetPracticeAreas'
  },
  'PAssociate': {
    'list': '/ProspectiveAssociate/GetPADetails',
    'create': '/ProspectiveAssociate/AddPADetails',
    'update': '/ProspectiveAssociate/UpdatePADetails',
    'get': '/ProspectiveAssociate/GetPADetailsByID?id=',
    'deletePA': '/ProspectiveAssociate/DeletePADetailsByID?empID='
  },

  'associates': {
    'getAssociateDetails': '/AssociatePersonalDetail/GetAssociateDetails',
    'getAssociateDetailsByEmpID': '/AssociatePersonalDetail/GetAssociateDetailsByEmpID?empID=',
    'create': '/ProspectiveAssociate/AddPADetails',
    'update': '',
    'get': '',
    'getJoinedAssociates': '/ProspectiveAssociate/GetJoinedAssociates'
  },
  'upload': {
    'list': '/FileUploadAndDownload/GetFileUploadedData?empID=',
    'GetPAstatus': '/ProspectiveAssociate/GetPAStatus?empID=',
    'save': '/FileUploadAndDownload/PostFile',
    'delete': '/FileUploadAndDownload/DeleteFileUploadedData',
    'submitForApproval': '/AssociatePersonalDetail/UpdateAssociateStatus?employeeId='
  },
  'associateprojects': {
    'get': '/AssociateSkills/GetAssociateProjectDetailsByID?empID=',
    'create': '/AssociateSkills/AddAssociateProjects',
    'update': '/AssociateSkills/UpdateAssociateProjectDetails'
  },
  'ProjectRoleAllocation': {
    'AssignProjectRole': '/ProjectRoleAssignment/AssignProjectRole',
    'GetRoleNotifications': '/ProjectRoleAssignment/GetRoleNotifications?employeeId=',
    'ApproveRole': '/ProjectRoleAssignment/RoleApproval',
    'GetAssignedRoles': '/ProjectRoleAssignment/GetAssignedRoles?projectId=',
    'RejectRole': '/ProjectRoleAssignment/RoleRejection',
    'GetProjectManagerOrLeadId': '/ProjectRoleAssignment/GetProjectManagerOrLeadId?employeeId=',
    'GetRolesByProjectId': '/ProjectRoleAssignment/GetRolesByProjectId?projectId=',
    'GetRoleDataById': '/ProjectRoleAssignment/GetRoleMasterDetailByRoleId?roleId='
  },
  'deliveryHead': {
    'getPendingRequisitionsForApproval': '/Dashboard/GetPendingRequisitionsForApproval?EmployeeId=',
    'getRolePositionDetailsByTRID': '/Dashboard/GetRolesAndPositionsByRequisitionId?talentRequisitionId=',
    'getTrRoleEmployeeList': '/Dashboard/GetTaggedEmployeeByTRAndRoleId?talentRequisitionId=',
    'getApproverList': '/Dashboard/GetFinanceHeadList',
    'ApproveTalentRequisition': '/Dashboard/ApproveTalentRequisition',
    'rejectTalentRequisition': '/TalentRequisition/RejectTalentRequisitionByDeliveryHead',
    'getProjectList': '/Project/GetProjectsList?userRole=',
    'ApproveOrRejectByDH' : '/Project/ApproveOrRejectByDH?projectId='
  },
  'FinanceHead': {
    'getPendingRequisitionsForApproval': '/Dashboard/GetRequisitionsForApprovalForFinance?employeeId=',
    'requisitionApprovalByFinance': '/Dashboard/ApproveTalentRequisitionByFinance',
    'requisitionRejectionByFinance': '/TalentRequisition/RejectTalentRequisitionByFinance'
  },
  'associateSkills': {
    'getSkillsList': '/AssociateSkills/GetSkills',
    'getSkillsById': '/AssociateSkills/GetDraftApproveStatusSkills?empId=',
    'getCompetencyAreas': '/AssociateSkills/GetCompetencyArea',
    'getPracticeAreas': '/PracticeArea/GetPracticeAreas',
    'getProficiencyLevels': '/AssociateSkills/GetProficiencyLevel',
    'SaveAssociateSkills': '/AssociateSkills/AddAssociateSkills',
    'DeleteAssociateSkills': '/AssociateSkills/DeleteAssociateSkill?id=',
    'getSkillsByCompetenctArea': '/AssociateSkills/GetSkills?competenctAreaID=',
    'getSkillsByCompetenctAreaAndSkillGroup': '/AssociateSkills/GetSkillsByID?skillGroupID=',
    'SubmitAssociateSkills': '/AssociateSkills/SubmitAssociateSkills',
  },

  // 'SkillSearch': {
  //   'getEmployeeDetailsBySkill': '/SkillSearch/getEmployeeDetailsBySkill/',
  //   'GetSkillsBySearchString': '/SkillSearch/GetSkillsBySearchString?searchString='
  // },
  // 'AssociateJoining': {
  //   'getJoinedAssociates': '/ProspectiveAssociate/GetJoinedAssociates'
  // },
  // 'AssociateInformation': {
  //   'getInfoAssociates': '/AssociatePersonalDetail/GetAssociateDetails'
  // },
  // "personal": {
  //   "getByEmpId": "/AssociatePersonalDetail/GetPersonalDetailsByID?empID=",
  //   "getByPAId": "/ProspectiveAssociate/GetPADetailsByID?ID=",
  //   "save": "/AssociatePersonalDetail/AddPersonalDetails",
  //   "update": "/AssociatePersonalDetail/UpdatePersonalDetails",
  //   "getManagersAndLeads": "/Project/GetManagersAndLeads",
  //   "getUserDepartmentDetails": "/UserDepartment/GetUserDepartmentDetails",
  //   "getDesignations": "/ProspectiveAssociate/GetDesignationByGrade?gradeID=",
  //   "getGradesDetails": "/UserGrade/GetGradesDetails",
  //   "getHRAdvisors": "/ProspectiveAssociate/GetHRAdvisors",
  //   "getEmpTypes": "/ProspectiveAssociate/GetEmpTypes",
  //   "getTechnologies": "/PracticeArea/GetPracticeAreas"
  // },
  // "PAssociate": {
  //   "list": "/ProspectiveAssociate/GetPADetails",
  //   "create": "/ProspectiveAssociate/AddPADetails",
  //   "update": "/ProspectiveAssociate/UpdatePADetails",
  //   "get": "/ProspectiveAssociate/GetPADetailsByID?id=",
  //   "deletePA": "/ProspectiveAssociate/DeletePADetailsByID?empID="
  // },
  // "associates": {
  //   "getAssociateDetails": "/AssociatePersonalDetail/GetAssociateDetails",
  //   "getAssociateDetailsByEmpID": "/AssociatePersonalDetail/GetAssociateDetailsByEmpID?empID=",
  //   "create": "/ProspectiveAssociate/AddPADetails",
  //   "update": "",
  //   "get": "",
  //   "getJoinedAssociates": "/ProspectiveAssociate/GetJoinedAssociates"
  // },
  // "upload": {
  //   "list": "/FileUploadAndDownload/GetFileUploadedData?empID=",
  //   "GetPAstatus": "/ProspectiveAssociate/GetPAStatus?empID=",
  //   "save": "/FileUploadAndDownload/PostFile",
  //   "delete": "/FileUploadAndDownload/DeleteFileUploadedData",
  //   "submitForApproval": "/AssociatePersonalDetail/UpdateAssociateStatus?employeeId="
  // },
  // "associateprojects": {
  //   "get": "/AssociateSkills/GetAssociateProjectDetailsByID?empID=",
  //   "create": "/AssociateSkills/AddAssociateProjects",
  //   "update": "/AssociateSkills/UpdateAssociateProjectDetails"
  // },

  'Reports': {
    'GetProjectsList': '/Dashboard/GetProjectsList',
    'GetFinanceReport': '/Reports/GetFinanceReports',
    'getFinanceReportToFreez': '/Reports/GetFinanceReportToFreez',
    'getRmgReportDataByMonthYear': '/Reports/GetRmgReportDataByMonthYear?month=',
    'GetUtilizationReportsByMonth': '/Reports/GetUtilizationReportsByMonth',
    'GetResourceReportByProjectId': '/Reports/GetResourceByProject?projectID=',
    'GetResourceReports': '/Reports/GetUtilizationReports',
    'GetUtilizationReportsByTechnologyId': '/Reports/GetUtilizationReportsByTechnology',
    'GetTalentpoolResourceCount': '/Reports/TalentPoolReportCount',
    'GetEmployeesByTalentPoolProjectID': '/Reports/TalentPoolReport?projectID=',
    'GetProjectsListByTypeId': '/Project/GetProjectsListByTypeId?projectTypeId=',
    'GetEmpList': '/Project/GetEmpList',
    'GetRolesList': '/Project/GetRolesList',
    'GetGradesDetails': '/UserGrade/GetGradesDetails',
    'GetCustomers': '/Project/GetCustomers',
    'GetDepartmentHeads': '/UserDepartment/GetDepartmentHeads',
    'GetManagers': '/Project/GetManagers',
    'GetProjectTypes': '/Project/GetProjectTypes',
    'GetActiveSkillsData': '/Dashboard/GetActiveSkillsData',
    'SaveReportData': '/Dashboard/SaveReportData',
    'ImportAssociateUtilizeReport': '/AssociateUtilization/ImportAssociateUtilizeReport',
    'UploadRMGReport': '/AssociateUtilization/ImportAssociateUtilizeReport',
    'GetDomainCountReport': '/Reports/DomainReportCount',
    'getEmployeesByDomainId': '/Reports/DomainReport?domainID=',
    'getEmployeesBySkill': '/Reports/SkillReport?competencyID=',
    'getEmployeesByTalentPoolProjectID': '/Reports/TalentPoolReport?projectID=',
    'GetEmployeesBySkill': '/Reports/GetAssociateSkillsReport',
    'GetSkillsBySearchString': '/AssociateSkills/GetSkillsBySearchString?searchString=',
    'GetProjectHistoryByEmployee': '/Reports/GetProjectHistoryByEmployee?employeeID='
  },

  'professional': {
    'getProfessionalDetails': '/AssociateProfessionalDetail/GetProfessionalDetailsByEmployeeID?employeeID=',
    'getSkillGroupCertificate': '/AssociateProfessionalDetail/GetSkillGroupsByCertificate',
    'addCertificationDetails': '/AssociateProfessionalDetail/AddCertificationDetails',
    'addMembershipDetails': '/AssociateProfessionalDetail/AddMembershipDetails',
    'updateCertificationDetails': '/AssociateProfessionalDetail/UpdateCertificationDetails',
    'updateMembershipDetails': '/AssociateProfessionalDetail/UpdateMembershipDetails',
    'deleteMembershipDetails': '/AssociateProfessionalDetail/DeleteProfessionalDetailsByID?id='
  },

  'education': {
    'list': '/AssociateEducationDetail/GetEducationDetailsByID',
    'save': '/AssociateEducationDetail/SaveEducationDetails'
  },

  'employment': {
    'GetEmploymentDetails': '/AssociateEmployment/GetEmploymentDetailsByID?empID=',
    'GetProfReferenceDetails': '/AssociateEmployment/GetProfReferenceDetailsByID?empID=',
    'SaveEmployementDetails': '/AssociateEmployment/SaveEmployementDetails'
  },
  'TagAssociate': {
    'GetTagListDetailsByManagerId': '/TagAssociate/GetTagListDetailsByManagerId?managerId=',
    'GetTagListNamesByManagerId': '/TagAssociate/GetTagListNamesByManagerId?managerId=',
    'CreateTagList': '/TagAssociate/CreateTagList/',
    'UpdateTagList': '/TagAssociate/UpdateTagAssociateList/',
    'DeleteTagAssociate': '/TagAssociate/DeleteTagAssociate?tagAssociateId=',
    'DeleteTagList': '/TagAssociate/DeleteTagList?tagListName='
  },
  'AssignReportingManager': {
    'GetManagersByProjectId': '/Project/GetReportingDetailsByProjectId',
    'GetProgramManagerList': '/Project/GetManagerList',
    'GetReportingManagersList': '/Project/GetAssociates',
    'AssignReportingManager': '/Project/SaveManagersToProject',
    'UpdateReportingManagerToAssociate': '/Project/UpdateReportingManagerToAssociate',
    'getManagerName': '/Project/GetManagerandLeadByProjectID?projectID=',
    'getManagersAndLeads': '/Project/GetManagersAndLeads',
    'getCurrentManagers': '/Project/GetReportingDetailsByProjectId?projectID='
  },
  'ResourceRelease': {
    'getTalentPools': '/Project/GetTalentPools',
    'getAssociates': '/Project/GetAssociates',
    'getProjectDetails': '/Project/GetProjectDetails?employeeId=',
    'releaseAssociate': '/Project/ReleaseAssociate',
    'getAssociateTalentPool': '/Project/GetEmpTalentPool?employeeId=',
    'GetAllocationPercentages': '/Common/GetAllocationPercentages',
    'GetEmployeesByProjectID': '/Project/GetEmployeesByProjectID?projectID=',
  },
  'family': {
    'list': '/AsociateFamilyDetail/GetFamilyDetailsByID?empID=',
    'save': '/AsociateFamilyDetail/UpdateFamilyDetails'
  },
  'EmployeeType': {
    'list': '/ProspectiveAssociate/GetEmpTypes'
  },
  'HRAdvisor': {
    'list': '/ProspectiveAssociate/GetHRAdvisors'
  },
  'TemporaryAllocationRelease': {
    'GetEmployeesAllocations': '/Allocation/GetEmployeesForAllocations',
    'TemporaryReleaseAssociate': '/Allocation/TemporaryReleaseAssociate',
    'GetAssociatesToRelease': '/Allocation/GetAssociatesToRelease?employeeId=',
    'GetAssociatePrimaryProject' : '/Allocation/GetEmployeePrimaryAllocationProject?employeeId='
  },
  'AssociateAllocation': {
    'GetApprovedTalentRequisitionList': '/Allocation/GetApprovedTalentRequisitionList',
    'GetTalentRequisitionById': '/Allocation/GetTalentRequisitionById?tRId=',
    'GetRequiredSkillsDetails': '/Allocation/GetRequiredSkillsDetails?requisitionRoleDetailId=',
    'ResourceAllocate': '/Project/ResourceAllocate',
    'GetReportingManager': '/Allocation/GetReportingManager',
    'GetReportingDetailsByProjectId': '/Project/GetReportingDetailsByProjectId?projectId=',
    'forceClose': '/Allocation/CloseTalentRequisition?tRId=',
    'GetMatchingProfiles': '/Allocation/GetMatchingProfiles?requisitionRoleId=',
    'GetMatchingskillsforAssociate': '/Allocation/GetMatchingskillsforAssociate?requisitionRoleId=',
    'CheckPrimaryRoleofAssociate': '/Allocation/CheckPrimaryRoleofAssociate?associateID=',
    'SetPrimaryRoleofAssociate': '/Allocation/SetPrimaryRoleofAssociate',
    'GetClientBillingRoles': '/ClientBillingRole/GetClientBillingRoles?isActive=true',
    'GetClientBillingRolesByProjectId': '/ClientBillingRole/GetClientBillingRolesByProjectId?projectId=',
    'GetInternalBillingRoles': '/InternalBillingRole/GetInternalBillingRoles?isActive=true',
    'GetTaggedEmployeesByTalentRequisitionId': '/Allocation/GetTaggedEmployeesByTalentRequisitionId?talentRequisitionId=',
    'GetEmpAllocationHistory': '/Project/GetEmpAllocationHistory?employeeId=',
    'GetTaggedEmployeesForTalentRequisition': '/Allocation/GetTaggedEmployeesForTalentRequisition',
    'GetEmployeePrimaryAllocationProject': '/Allocation/GetEmployeePrimaryAllocationProject?EmployeeId=',
    'getProjectsForAllocation': '/Allocation/GetProjectsForAllocation'
  },

  'ADROrganisationDevelopment':{
    'get' :'/ADROrganisationalDevelopment/GetADROrganisationDevelopment?financialYearId=',
    'create': '/ADROrganisationalDevelopment/CreateADROrganisationDevelopment',
    'update' :'/ADROrganisationalDevelopment/UpdateADROrganisationDevelopment?financialYearId=',
    "getCurrentFinancialYear": "/MasterData/GetCurrentFinancialYear"
  },

  'ADROrganisationValue':{
    'get' :'/ADROrganisationValue/GetADROrganisationValues?financialYearId=',
    'create': '/ADROrganisationValue/CreateADROrganisationValue',
    'update' :'/ADROrganisationValue/UpdateADROrganisationValue?financialYearId=',
    "getCurrentFinancialYear": "/MasterData/GetCurrentFinancialYear",
  },

  'ADRConfiguration':{
    'get' :'/ADRConfiguration/GetADRConfiguration',
    'create': '/ADRConfiguration/CreateADRConfiguration',
    'update' :'/ADRConfiguration/UpdateADRConfiguration'
  },

  'ADRSections' : {
    'get' :'/ADR/GetADRSection',
    'create': '/ADR/CreateADRSection',
    'update' :'/ADR/UpdateADRSection',
    'getADRMeasurementAreas' : '/ADR/GetCurrentYearADRMeasurementAreas'
  },

  "AssociateDevelopmentReview": {
    "GetADRCycleList": "/MasterData/GetADRCycle",
    "UpdateADRCycle": "/ADR/SetADRCycleActive"
  },
  
  "ADRAssociateAppreciation": {
    "GetSentAppreciationsList": "/Appreciation/GetAppreciationHistory?empID=",
    "GetReceiveAppreciationsList": "/Appreciation/GetMyAppreciation?empID=",
    "GetADRCycleList": "/MasterData/GetADRCycle",
    "GetAppreciationTypeList": "/Appreciation/GetAppreciationTypeList",
    "GetFinancialYearList": "/MasterData/GetFinancialYearList",
    "SendAnAppreciation": "/Appreciation/CreateAppreciation",
    "UpdateAnAppreciation": "/Appreciation/UpdateAnAppreciation",
    "DeleteAnAppreciation": "/Appreciation/DeleteAnAppreciation"
  },

};
