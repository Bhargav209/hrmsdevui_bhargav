export class CloneKRA {
    FromFinancialYearId: number;
    ToFinancialYearId: number;
    DepartmentId:number;
    DepartmentIds: number[];
    GroupIds: number[];
    CloneType: number;
  }
  