export class SkillData{
    SkillId:number;
    SkillCode:string;
    SkillDescription:string;
    SkillGroupId:number;
    SkillGroupName: string;
    CompetencyAreaId:number;
    CompetencyAreaCode:string;
}