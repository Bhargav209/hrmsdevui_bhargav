export interface AssociateSkill {
    // RoleId?: any;
    CompetencyAreaID?: any;
    PracticeAreaID?: any;
    skills: any[];
    skillList: any[];
    skillsVisible: boolean;
  }
  
  export interface CompetencyArea {
    CompetencyAreaId?: number;
    CompetencyAreaCode: string;
    CompetencyAreaDescription: string;
    IsActive?: boolean;
  }
  
  export interface PracticeArea {
    PracticeAreaId?: number;
    PracticeAreaCode: string;
    PracticeAreaDescription: string;
    // IsActive?: boolean;
  }
  
  export interface ProficiencyLevel {
    ProficiencyLevelId?: number;
    ProficiencyLevelCode: string;
    ProficiencyLevelDescription: string;
    IsActive?: boolean;
  }
  
  export interface Role {
    ID: number;
    Name: string;
  }
  
  export class Skill {
    ID?: number;
    empID?: number;
    skillId?: number;
    SkillName: string;
    proficiencyLevelId?: number;
    experience?: number;
    LastUsed?: number;
    isPrimary?: boolean;
    CompetencyAreaId?: number;
    SkillGroupId?: number;
    skillID?: number;
    RoleId : number;
    StatusCode : string;
    CompetencyAreaID : number;
    SkillGroupID : number;
  }
  
  export class EmployeeSkillDetails{
    skillDetails : Array<Skill>;
    ID?: number;
    empID?: number;
    RoleId : number;
  }
  export class SkillGroup {
    SkillGroupId?: number;
    SkillGroupName: string;
    Description: string;
  }
  