﻿import { GenericType } from "./dropdowntype.model";

// import { GenericType } from '../models/index'


export class DepartmentDetails{
  DepartmentId: number;
      DepartmentCode?: string
      Description?: string
}

export class RoleData extends DepartmentDetails{
      ID?: number;
      SGRoleID: number;
      RoleMasterId?: number;
       KRAGroupId: number;
      KRATitle: string;
      RoleName: string;
      RoleDescription: string;
      KeyResponsibilities: string;
      EducationQualification: string;
      PrefixID: number;
      SuffixID: number
      dateOfJoining?: Date;
      RoleCompetencySkills?: RoleCompetencySkills[];
      DeltedSkillIds?: any[];
      IsActive?: boolean;
}

export class RoleSufixPrefix {
      Roles?: GenericType[]
      Prefix?: GenericType[]
      Suffix?: GenericType[]
}

export class RoleCompetencySkills {
      ID?: number;
      CompetencySkillsId?: number;
      CompetencyAreaId?: number;
      SkillGroupId?: number;
      SkillGroupName?: string;
      SkillId?: number;
      RoleMasterID?: number;
      IsPrimary: boolean;
      ProficiencyLevelId?: number;
      CompetencyAreaCode?: string;
      SkillName?: string;
      ProficiencyLevelCode: string;
}


    //  DeltedSkillIds:
