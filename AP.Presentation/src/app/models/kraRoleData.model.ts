export class KraRoleData {
    RoleID: number;
    CloneRoleID: number;
    DepartmentId: number;
    CloneFinancialYearID: number;
    FinancialYearID: number;
    KRAAspectID: number;
    KRAAspectMetric: string;
    KRAAspectTarget: string;
    ID: number;
    KRARoleID : number;
    KRARoleName : string;
}