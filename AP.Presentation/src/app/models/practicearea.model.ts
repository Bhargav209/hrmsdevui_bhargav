export class PracticeArea {
    PracticeAreaId?: number;
    PracticeAreaCode: string;
    PracticeAreaDescription: string;
    IsActive?: boolean;
  }