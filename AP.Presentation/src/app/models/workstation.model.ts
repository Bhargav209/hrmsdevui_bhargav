export class WorkStationDataCount {
    WorkStationCount: number;
    Name: string;
}
export class WorkStation {
    IsOccupied: boolean;
    BayId: number;
    EmployeeId: number;
    Id: number;
    WorkStationId: number;
    AssociateName: string;
    LeadName: string;
    ProjectName: string;
    SystemInfo: string;
    WorkStationCode: string;
}
export class BayInformation {
    BayId: number;
    Name: string;
    Description: string;
}
