export class AssociateRoleMappingData {
  Id :number;
  AssociateName :string;
  RoleName :string;
  DepartmentId: number;
  ProjectId:number;
  RoleMasterId : number;
  IDs : number[];
  FinancialYearID  : number;
  KRAGroupId : number;
  KRARoleId : number;
  EmployeeName : string;
  EmployeeId : number;
}

