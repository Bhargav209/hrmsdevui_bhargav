import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/assignmanagertoproject.model';

@Injectable({
  providedIn: 'root'
})
export class AssignReportingManagerService {
  serviceUrl = environment.ServerUrl;
  resources = servicePath.API.AssignReportingManager;
  constructor(private httpClient: HttpClient) { }
 
  public AssignReportingManager(employee:Employee, isDelivery:boolean) 
  {
    let _url = this.serviceUrl + this.resources.UpdateReportingManagerToAssociate + "?isDelivery=" + isDelivery;
    return this.httpClient.post(_url, employee);     
  }
  public GetManagerName(projectId: number, employeeId: number) 
  {
    var url = this.serviceUrl + this.resources.getManagerName + projectId +  "&employeeId="  + employeeId;
    return this.httpClient.get(url)
  }
  
}
