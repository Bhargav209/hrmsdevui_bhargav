import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as servicePath from '../../../service-paths';
import { environment } from '../../../../environments/environment';
import { ProjectDetails } from '../../../models/projects.model';
import { SOW } from '../../admin/models/sow.model';


@Injectable({
  providedIn: 'root'
})
export class SowService {
  serviceUrl = environment.ServerUrl;
  resources = servicePath.API.sow;
  constructor(private httpClient: HttpClient) { }
  SOWData : SOW[] = [];

  public SaveSOW(newSOW){ // data type
    let url = this.serviceUrl + this.resources.createSOW;
    return this.httpClient.post(url,newSOW ) 
  }
  public GetSowByProjectId(projectId: number) {
    var url = this.serviceUrl + this.resources.getSOWDetailsById+projectId;
    return this.httpClient.get(url )
}
public GetProjectsList(userRole : string, empId : number) {
  let _url = this.serviceUrl + servicePath.API.projects.getProjectList + userRole +"&employeeId="+ empId;
  return this.httpClient.get<ProjectDetails[]>(_url)
  } 
  public CreateAddendum(newAddendum){
    let url = this.serviceUrl + this.resources.createSOW;
    return this.httpClient.post(url,newAddendum ) 
  }
  public GetAddendumsBySOWId(ID: number , projectId : number) {
    var url = this.serviceUrl + this.resources.GetAddendumsBySOWId+ID + "&projectId=" + projectId;
    return this.httpClient.get(url)
}
public GetSowDetails(projectId : number, Id : number , roleName : string){
    var url = this.serviceUrl + this.resources.GetSowDetails+Id + "&projectId=" + projectId + "&roleName=" + roleName;
    return this.httpClient.get(url )
}

public GetAddendumDetailsById(projectId : number, Id : number , roleName : string){
    var url = this.serviceUrl + this.resources.GetSddendumDetailsById+Id + "&projectId=" + projectId + "&roleName=" + roleName;
    return this.httpClient.get(url )
}

public UpdateSOWAndAddendumDetails(updateSowObj){
    let url = this.serviceUrl + this.resources.UpdateSOWAndAddendumDetails;
    return this.httpClient.post(url,updateSowObj ) 
}

public DeleteSow(Id){
  let url = this.serviceUrl + this.resources.delete + Id;
  return this.httpClient.post(url,null ) 
}
}
