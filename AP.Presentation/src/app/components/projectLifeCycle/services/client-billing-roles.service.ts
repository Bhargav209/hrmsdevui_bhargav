import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as servicePath from '../../../service-paths';
import { environment } from '../../../../environments/environment';
import { ClientBillingRoleDetails } from '../models/client-billing-role.model';

@Injectable({
    providedIn: 'root'
})
export class ClientBillingRoleService {
    serviceUrl = environment.ServerUrl;
    resources = servicePath.API.ClientBillingRole;
    constructor(private httpClient: HttpClient) { }

    public SaveClientBillingRole(clientBillingRoleData: ClientBillingRoleDetails) {
        let url = this.serviceUrl + this.resources.create;
        return this.httpClient.post(url, clientBillingRoleData);
    }

    public UpdateClientBillingRole(clientBillingRoleData: ClientBillingRoleDetails) {
        let url = this.serviceUrl + this.resources.update;
        return this.httpClient.post(url, clientBillingRoleData);
    }

    public GetClientBillingRolesByProjectId(projectId: number) {
        var url = this.serviceUrl + this.resources.getClientBillingRolesByProjectId + projectId;
        return this.httpClient.get(url);
    }

    public DeleteClientBillingRole(clientBillingRoleId: number) {
        let url = this.serviceUrl + this.resources.delete + clientBillingRoleId;
        return this.httpClient.post(url, null);
    }
    public closeClientBillingRecord(cBRId : number , endDate ){
        let url = this.serviceUrl + this.resources.CloseClientBillingRole + cBRId + "&endDate=" + endDate;
        
        return this.httpClient.post(url, null)
    }


}
