export class TagAssociateList{
    EmployeeId:number;    
    Id:number;
    ManagerId:number;
    TagListId:number;
    Allocation : number;
    TagListName:string;
    EmployeeName:string;
    Designation:string;
}