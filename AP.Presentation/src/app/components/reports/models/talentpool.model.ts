export class TalentpoolDataCount {
    ResourceCount: number;
    ProjectID: number;
    ProjectName: string
}

export class TalentPoolReportData {
    EmployeeId : number;
    EmployeeCode : string;
    ProjectName: string;
    ClientName : string;
    RoleName : string;
    Technology : string;
    Billable: boolean = false;
    FromDate : string;
    ToDate : string;
    EmployeeName : string;
    Designation : string;
    Grade : string;
    Experience : number;
}