export class  SkillReportData{
    EmployeeCode: string;
    EmployeeName : number;
    CompetencyArea : string;
    SkillGroup : string;
    skill:string;
    LastUsed: number;
    IsPrimary:boolean;
    ProficiencyLevel:string;
}