import {  ReportsFilterData } from '../models/reportsfilter.model';
import { AllocationDetails } from '../models/resourcereportbyproject.model';
import { Observable } from 'rxjs';
export interface IResourceReports {
    ResourceReportByFilters(resourceFilter: ReportsFilterData): Observable<ReportsFilterData>
    GetResourceReportByProjectId(projectId: number): Observable<AllocationDetails>
    GetUtilizationReportsByTechnology(resourceFilter: ReportsFilterData): Observable<ReportsFilterData>
    GetUtilizationReportsByMonth(resourceFilter: ReportsFilterData): Observable<ReportsFilterData>


}
