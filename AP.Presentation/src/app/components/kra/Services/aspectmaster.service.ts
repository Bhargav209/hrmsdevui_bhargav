import { Injectable, Inject } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { AspectData } from "../../../models/kra.model";
import { IAspectMaster } from "../../../Interfaces/IAspectMaster";
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';

@Injectable({  
  providedIn: 'root'
})

export class AspectMasterService implements IAspectMaster {
  private _resources: any;
  private _serverURL: string;

  constructor(private _http: HttpClient) { this._serverURL = environment.ServerUrl; this._resources = servicePath.API.AspectMaster;}

  public GetAspectMasterList(): Observable<AspectData[]> {
    var url = this._serverURL + this._resources.GetAspectMasterList;
    return this._http.get<AspectData[]>(url);
  }

  public CreateAspect(aspectData: AspectData): Observable<number> {
    let _url = this._serverURL + this._resources.CreateAspect;
    return this._http.post<number>(_url, aspectData);
  }

  public UpdateAspect(aspectData: AspectData): Observable<number> {
    let _url = this._serverURL + this._resources.UpdateAspect;
    return this._http.post<number>(_url, aspectData);
  }

   public DeleteAspect(aspectId: number): Observable<number> {
    let _url = this._serverURL + this._resources.DeleteAspect + aspectId;
    return this._http.post<number>(_url, null);
  }

}
