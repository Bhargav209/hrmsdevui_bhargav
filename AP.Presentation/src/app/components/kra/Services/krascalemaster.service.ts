
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';
import { KRAScaleMaster, KRAScaleDetails } from "../../../models/krascaleData.model";
import { IKRAScaleMaster } from "../../../Interfaces/IKRAScaleMaster";

@Injectable({
  providedIn: 'root'
})
export class KrascalemasterService implements IKRAScaleMaster {
  private _resources: any;
  private _serverURL: string;

  constructor(private _http: HttpClient) { this._serverURL = environment.ServerUrl; this._resources = servicePath.API.KRAScaleMaster;}

  public GetKRAScale(): Observable<Array<KRAScaleMaster>> {
    let url = this._serverURL + this._resources.getKRAScale;
    return this._http.get<Array<KRAScaleMaster>>(url);
  }
  public GetKRADescriptionDetails(kraScaleMasterId:number):Observable<Array<KRAScaleDetails>>{
    let url = this._serverURL + this._resources.getKRADescriptionDetails + kraScaleMasterId;
    return this._http.get<Array<KRAScaleDetails>>(url);
  }

  public CreateKRAScale(kraScaleMaster: KRAScaleMaster): Observable<number> {
    let _url = this._serverURL + this._resources.createKRAScale;
    return this._http.post<number>(_url, kraScaleMaster);
  }
  public UpdateKRAScale(kraScaleDetails: KRAScaleDetails[], maxScale:number): Observable<number> {
    let _url = this._serverURL + this._resources.updateKRAScale + maxScale;
    return this._http
      .post<number>(_url, kraScaleDetails);
     
  }
  public DeleteKRAScale(kraScaleMasterId: number): Observable<number> {
   let _url = this._serverURL + this._resources.deleteKRAScale + kraScaleMasterId;
        return this._http.post<number>(_url, null);
  }

}
