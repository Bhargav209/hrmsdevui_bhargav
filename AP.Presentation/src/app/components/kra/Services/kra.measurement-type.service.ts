import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { KRAMeasurementTypeData } from 'src/app/models/kra.measurement-type.model';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';

@Injectable({ providedIn: 'root' })

export class KraMeasurementTypeService {
    private _resources: any;
    private _serverUrl: string;
    constructor(private _http: HttpClient) {
      this._serverUrl = environment.ServerUrl;
      this._resources = servicePath.API.KraMeasurementType;
    }

    public DeleteKraMeasurementType(KraMeasurementTypeId: number) {
        let url = this._serverUrl + this._resources.deleteKraMeasurementType + KraMeasurementTypeId;
        return this._http.post(url, KraMeasurementTypeId); 
    }

    public CreateKraMeasurementType(kraMeasurementTypeData: KRAMeasurementTypeData) {
        let url = this._serverUrl + this._resources.createKraMeasurementType;
        return this._http.post(url, kraMeasurementTypeData); 
    }

    public UpdateKraMeasurementType(kraMeasurementTypeData: KRAMeasurementTypeData) {
        let url = this._serverUrl + this._resources.updateKraMeasurementType;
        return this._http.post(url, kraMeasurementTypeData);
    }
}