import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import * as servicePath from '../../../service-paths';
import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { KrascalemasterService } from "../Services/krascalemaster.service";
import { KRAScaleMaster, KRAScaleDetails } from '../../../models/krascaleData.model';
import { GenericType } from '../../../models/dropdowntype.model';
import { MasterDataService } from '../../../services/masterdata.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-kra-scale-master',
  templateUrl: './kra-scale-master.component.html',
  styleUrls: ['./kra-scale-master.component.scss'],
  providers: [KrascalemasterService, ConfirmationService]
})
export class KraScaleMasterComponent implements OnInit {
  resources = servicePath.API.PagingConfigValue;

  private componentName: string;
  public PageSize: number;
  public PageDropDown: number[];


  public kraMasterScaleList: KRAScaleMaster[] = [];
  public kraMasterScale: KRAScaleMaster;

  private tempKRAScaleDetails: KRAScaleDetails[] = [];
  private scaleDesciptionList: GenericType[];
  public KRAScaleTitle: string = "Add KRA Scale";
  public saveButton: string = "Save";
  public kraScaleDisplay: boolean = false;
  public kraDescriptionView: boolean = false;
  public formSubmitted: boolean = false;
  public myForm: FormGroup;
  public descriptionView: string = "";
  public descriptionHide: boolean = false;
  private minimumScaleValidation: boolean = true;
  private maximumScaleValidation: boolean = true;
  public spaceValidation: boolean = false;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _kraScaleMasterService: KrascalemasterService,
    private _fb: FormBuilder,
    private _confirmationService: ConfirmationService,
    private _masterDataService: MasterDataService,
    private messageService: MessageService

  ) {
    this.componentName = this._activatedRoute.routeConfig.component.name;
    this.PageSize = this.resources.PageSize;
    this.PageDropDown = this.resources.PageDropDown;
  }

  ngOnInit() {

    this.resetForm();
    this.getKRAScaleList();
    this.getKRAScaleDescriptions();
    this.myForm = this._fb.group({
      scaleTitle: [
        "",
        [Validators.required, Validators.pattern("^[a-zA-Z0-9-_ ]*$")]
      ],
      minimumScale: [null, [Validators.required]],
      maximumScale: [null, [Validators.required]],
      scaleDescriptions: this._fb.array([this.initDescription()])
    });
  }
  cols = [
    { field: 'KRAScaleTitle', header: 'Scale Title' },
    { field: 'MinimumScale', header: 'Minimum Scale' ,type : "number"},
    { field: 'MaximumScale', header: 'Maximum Scale', type : "number" },

  ];

  initDescription() {
    return this._fb.group({
      descriptionDetailId: [null],
      descriptionValue: [null],
      descriptionName: [
        "",
        [Validators.required, Validators.pattern("^[a-zA-Z0-9_ ]*$")]
      ]
    });
  }

  private getKRAScaleDescriptions(): void {
    this._masterDataService.getKRAScaleValues()
      .subscribe((desciptionResponse: GenericType[]) => {
        this.scaleDesciptionList = [];
        this.scaleDesciptionList = desciptionResponse;
      });

  }
public getStyles(type : string){
  if(type == 'number'){
    return { 'text-align' : 'right', 'width':'120px'};
  }
}
  private getKRAScaleList(): void {
    this._kraScaleMasterService.GetKRAScale().subscribe(
      (scaleResponse: KRAScaleMaster[]) => {
        this.kraMasterScaleList = [];
        this.kraMasterScaleList = scaleResponse;
      },
      (error: any) => {
        if (error._body != undefined && error._body != "")
        this.messageService.add({
          severity: 'error',
          summary: 'Error Message',
          detail: 'Failed to get KRA Scale List'
        });
      }
    );
  }

  public onAddKRAScale(): void {
    this.descriptionHide = false;
    this.saveButton = "Save";
    this.KRAScaleTitle = "Add KRA Scale";
    this.resetForm();
    this.kraScaleDisplay = true;
  }

  private onMinimunScaleChange(
    minimumScale: number,
    MaximumScale: number
  ): void {
    if (minimumScale != null && minimumScale != 1) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning Message',
        detail: 'Minimum Scale should start from 1'
      });
      this.minimumScaleValidation = false;
      return;
    } else {
      this.minimumScaleValidation = true;
    }
    if (minimumScale != null && MaximumScale != null) {
      this.onMaximumScaleChange(minimumScale, MaximumScale);
    }
  }

  private onMaximumScaleChange(
    minimumScale: number,
    maximumScale: number
  ): void {
    this.formSubmitted = false;
    if (minimumScale != null && maximumScale != null) {
      if (!this.minimumScaleValidation) return;
      if (minimumScale >= maximumScale) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning Message',
          detail: 'Maximum Scale should be greater than or equals to Minimum Scale'
        });
        
        this.maximumScaleValidation = false;
        return;
      } else if (maximumScale > 30) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning Message',
          detail: 'Maximum Scale should not be greater than 30'
        });
        this.maximumScaleValidation = false;
        return;
      } else {
        this.maximumScaleValidation = true;
        this.myForm.controls["scaleDescriptions"] = new FormArray([]);
        const control = <FormArray>this.myForm.controls["scaleDescriptions"];
        (control: FormArray) => {
          while (control.length !== 0) {
            control.controls = [];
            this.descriptionHide = false;
          }
        };
        for (let i = minimumScale; i <= maximumScale; i++) {
          control.push(this.initDescription());
        }
        this.descriptionHide = true;
      }
    }
  }

  public saveKRAScale(kraMasterScale: KRAScaleMaster): void {
    this.formSubmitted = true;
    if (kraMasterScale.KRAScaleTitle) {
      kraMasterScale.KRAScaleTitle = kraMasterScale.KRAScaleTitle.trim().replace(/  +/g, ' ');
      if (kraMasterScale.KRAScaleTitle == '') {
        this.spaceValidation = true;
        // when we are displaying error mesage no need for toaster --( removed by hema suggested by sravanthi.)
        return;
      } else {
        this.spaceValidation = false;
      }
    } else {
      this.spaceValidation = false;
    }
    if (
      kraMasterScale.KRAScaleTitle == "" ||
      kraMasterScale.MaximumScale == null ||
      kraMasterScale.MaximumScale == null ||
      !this.myForm.controls["scaleTitle"].valid
    )
      return;
    if (!this.minimumScaleValidation) {
      this.onMinimunScaleChange(
        kraMasterScale.MinimumScale,
        kraMasterScale.MaximumScale
      );
      return;
    }
    if (!this.maximumScaleValidation) {
      this.onMaximumScaleChange(
        kraMasterScale.MinimumScale,
        kraMasterScale.MaximumScale
      );
      return;
    }
    let scaleDescription: any[] = [];
    let numberRegex = /^[a-zA-Z0-9_\s]*$/;
    var desc = '';;
    scaleDescription = this.myForm.controls["scaleDescriptions"].value;
    if (scaleDescription.length == 0) {
      return;
    }
    else {
      let description: any[] = [];

      for (let i = 0; i < scaleDescription.length; i++) {

        if (scaleDescription[i].descriptionName)
          scaleDescription[i].descriptionName = scaleDescription[i].descriptionName.trim().replace(/  +/g, ' ');
        let descriptionValidation = numberRegex.test(
          scaleDescription[i].descriptionName
        );

        description[i] = (scaleDescription[i].descriptionName == "" || !descriptionValidation || scaleDescription[i].descriptionName == null)

        if (description[i] == true) {
          desc = 'false';
        }
        else desc = 'true';

      }
   
      if (desc == 'true') {
        kraMasterScale.KRAScaleDetails = scaleDescription.map(
          (scaleDescription, index) => {
            if (this.tempKRAScaleDetails.length > 0) {
              return {
                ScaleDescription: scaleDescription.descriptionName,
                KRAScale: this.tempKRAScaleDetails[index].KRAScale,
                KRAScaleDetailId: this.tempKRAScaleDetails[index]
                  .KRAScaleDetailId
              };
            } else {
              return {
                ScaleDescription: scaleDescription.descriptionName
              };
            }
          }
        );
      } else {
        // this.messageService.add({
        //   severity: 'warn',
        //   summary: 'Warning Message',
        //   detail: 'Enter Valid input'
        // });
        return;
      }
    }
    if (this.saveButton == "Save") {
      for (
        let i = kraMasterScale.MinimumScale;
        i <= kraMasterScale.MaximumScale;
        i++
      )
        if (kraMasterScale.KRAScaleDetails.length > 0)
          kraMasterScale.KRAScaleDetails[i - 1].KRAScale = i;
      this._kraScaleMasterService.CreateKRAScale(kraMasterScale).subscribe(
        (response: number) => {
          if (response == 1) {
            this.kraScaleDisplay = false;
            this.messageService.add({
              severity: 'success',
              summary: 'Success Message',
              detail: 'KRA Scale saved Successfully'
            });
            this.getKRAScaleList();
            this.getKRAScaleDescriptions();
          } else if (response == -1) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Warning Message',
              detail: 'Either Scale Title or Descriptions may be duplicate'
            });
          } else if (response == -13) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Warning Message',
              detail: 'Enter valid input'
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: 'Error Message',
              detail: 'Failed to Save KRA Scale'
            });
          }
        },
        (error: any) => {
          if (error._body != undefined && error._body != "")
          this.messageService.add({
            severity: 'error',
            summary: 'Error Message',
            detail: 'Failed to Save KRA Scale'
          });
        }
      );
    } else {
      this._kraScaleMasterService.UpdateKRAScale(
        kraMasterScale.KRAScaleDetails,
        kraMasterScale.MaximumScale
      )
        .subscribe(
          (response: number) => {
            if (response == 1) {
              this.kraScaleDisplay = false;
              this.messageService.add({
                severity: 'success',
                summary: 'Success Message',
                detail: 'KRA Scale updated Successfully'
              });
              this.getKRAScaleList();
              this.getKRAScaleDescriptions();
            } else if (response == -1) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Warning Message',
                detail: 'Either Scale Title or Descriptions may be duplicate'
              });
            }
            else if (response == -13) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Warning Message',
                detail: 'Enter valid input'
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: 'Error Message',
                detail: 'Failed to Update KRA Scale'
              });
            }
          },
        (error: any) => {
          if (error._body != undefined && error._body != "")
          this.messageService.add({
            severity: 'error',
            summary: 'Error Message',
            detail: 'Failed to Update KRA Scale'
          });
        }
      );
    }
  }

  private onEditKRAScaleDescription(kraScaleMaster: KRAScaleMaster): void {
    this.onMaximumScaleChange(
      kraScaleMaster.MinimumScale,
      kraScaleMaster.MaximumScale
    );
    this.kraScaleDisplay = true;
    this.formSubmitted = false;
    this.saveButton = "Update";
    this.KRAScaleTitle = "Update KRA Scale Descriptions";
    this._kraScaleMasterService
      .GetKRADescriptionDetails(kraScaleMaster.KRAScaleMasterID)
      .subscribe(
        (DescriptionResponse: KRAScaleDetails[]) => {
          this.kraMasterScale = kraScaleMaster;
          this.kraMasterScale.KRAScaleDetails = DescriptionResponse;
          this.tempKRAScaleDetails = [];
          this.tempKRAScaleDetails = DescriptionResponse;
          DescriptionResponse.forEach(
            (response: KRAScaleDetails, index: number) => {
              const fbScaleDescriptions = this.myForm.get(
                "scaleDescriptions"
              ) as FormArray;
              fbScaleDescriptions.at(index).setValue({
                descriptionDetailId: response.KRAScaleDetailId,
                descriptionValue: response.KRAScale,
                descriptionName: response.ScaleDescription
              });
            }
          );
        },
      (error: any) => {
        if (error._body != undefined && error._body != "")
        this.messageService.add({
          severity: 'error',
          summary: 'Error Message',
          detail: 'Failed to get KRA Scale Description'
        });
      }
    );
  }

  private onDeleteKRAScale(kraScaleMaster: KRAScaleMaster): void {
    this._confirmationService.confirm({
      message: "Are you sure, you want to delete?",
      header: "KRA Scale Delete",
      key: "deleteScaleConfirmation",
      icon: "fa fa-trash",
      accept: () => {
        this._kraScaleMasterService
          .DeleteKRAScale(kraScaleMaster.KRAScaleMasterID)
          .subscribe(
            (scaleResponse: number) => {
              if (scaleResponse == 1) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Success Message',
                  detail: 'KRA Scale deleted Successfully'
                });
                this.getKRAScaleList();
                this.getKRAScaleDescriptions();
              } else if (scaleResponse == 4) {
                this.messageService.add({
                  severity: 'warn',
                  summary: 'Warning Message',
                  detail: 'Cannot delete this KRA Scale as KRA definitions are assigned'
                });
              } else {
                this.messageService.add({
                  severity: 'error',
                  summary: 'Error Message',
                  detail: 'Failed to delete KRA Scale'
                });
              }
            },
            (error: any) => {
              if (error._body != undefined && error._body != "")
              this.messageService.add({
                severity: 'error',
                summary: 'Error Message',
                detail: 'Failed to delete KRA Scale'
              });
            }
          );
      },
      reject: () => {
        return;
      }
    });
  }

  private onViewKRAScaleDescription(kraScaleMaster: KRAScaleMaster): void {
    this.kraDescriptionView = true;
    let descriptionList: GenericType[] = [];
    this.KRAScaleTitle = "View KRA Scale Descriptions";
    if (this.scaleDesciptionList.length > 0 && kraScaleMaster.KRAScaleMasterID) {

      for (let i = 0; i < this.scaleDesciptionList.length; i++) {
        let scaleDesciption = this.scaleDesciptionList[i];

        if (scaleDesciption.Id == kraScaleMaster.KRAScaleMasterID) {
          descriptionList.push(scaleDesciption);
        }
      }
      if (descriptionList.length > 0) {
        this.descriptionView = descriptionList[0].Name;
      } else {
        this.descriptionView = "No Records";
      }

    }
  }

  public resetForm(): void {
    this.spaceValidation = false;
    this.formSubmitted = false;
    if (this.saveButton == "Save") {
      this.descriptionHide = false;
      this.kraMasterScale = new KRAScaleMaster();
      this.kraMasterScale = {
        KRAScaleMasterID: 0,
        MinimumScale: 1,
        MaximumScale: null,
        KRAScaleTitle: "",
        KRAScaleDetails: new Array<KRAScaleDetails>()
      };
      this.maximumScaleValidation = true;
      this.minimumScaleValidation = true;
    } else {
      this.kraMasterScale.KRAScaleDetails = [];
      this.myForm.controls["scaleDescriptions"].reset();
    }
  }

  public cancelDialog(): void {
    this.kraScaleDisplay = false;
  }

}
