import { Component, OnInit } from '@angular/core';
import { MasterDataService } from 'src/app/services/masterdata.service';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { KraMeasurementTypeService } from '../Services/kra.measurement-type.service';
import { DropDownType, GenericType } from 'src/app/models/dropdowntype.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { KRAMeasurementTypeData } from 'src/app/models/kra.measurement-type.model';
import { ActivatedRoute } from '@angular/router';
import * as servicePath from '../../../service-paths';

@Component({
  selector: 'app-kra.measurement-type',
  templateUrl: './kra.measurement-type.component.html',
  styleUrls: ['./kra.measurement-type.component.scss'],
  providers: [MasterDataService, ConfirmationService, KraMeasurementTypeService]
})

export class KraMeasurementTypeComponent {
  resources = servicePath.API.PagingConfigValue;
  errorMessage: Message[] = [];
  componentName: string;
  measureTypeList: DropDownType[] = [];
  buttonTitle: string;
  measurementForm: FormGroup;
  PageSize: number;
  PageDropDown: number[] = [];
  measurementFormSubmitted: boolean;
  measurementType: string;
  kraMeasurementTypeData: KRAMeasurementTypeData;
  valid: boolean = false;

  constructor(
    private _masterDataService: MasterDataService, 
    private _actRoute: ActivatedRoute, 
    private _formBuilder: FormBuilder, 
    private _kraMeasurementTypeService: KraMeasurementTypeService, 
    private _confirmationService: ConfirmationService,
    private messageService: MessageService) {
      this.componentName = this._actRoute.routeConfig.component.name;
      this.buttonTitle = "Add";
      this.PageSize = this.resources.PageSize;
      this.PageDropDown = this.resources.PageDropDown;
      this.kraMeasurementTypeData = new KRAMeasurementTypeData();
      
  }

  ngOnInit() {
      this.measurementForm = this._formBuilder.group({
          measurementType: ["", Validators.required]
      });
      this.getMeasurementTypes();
  }

  cols = [
    { field: 'label', header: 'Measurement Type Name' }
  ];

  omitSpecialChar(event: any) {
      let k: any;
      k = event.charCode;
      return (k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 44 || k == 45 || k == 38;
  }

  public validateMeasurementType(measurementType: string) {
      let numberRegex = /^[a-zA-Z-&,\s]*$/;
      this.valid = numberRegex.test(measurementType);
      if (!this.valid) {
          this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'Enter valid Measurement Type.' });
          return;
      }
  }

  private getMeasurementTypes(): void {
      this._masterDataService.GetKRAMeasurementType().subscribe((res: GenericType[]) => {
          this.measureTypeList = [];
          res.forEach(element => {
              this.measureTypeList.push({ label: element.Name, value: element.Id });
          });
      },
          (error: any) => {
              this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Failed to get Measurement Type.' });
          }
      );
  }

  private createKraMeasurementType() {
      this.measurementFormSubmitted = true;
      if (this.measurementForm.valid) {
          if (this.measurementType != undefined && this.measurementType != "") {
              let position: number = this.measurementType.indexOf('-', 0);
              let position1: number = this.measurementType.indexOf('&', 0);
              let position2: number = this.measurementType.indexOf(',', 0)
              if (this.measurementType.trim().length == 0
                  || !this.valid || this.measurementType.charAt(position) == this.measurementType.charAt(position + 1) || this.measurementType.charAt(position1) == this.measurementType.charAt(position1 + 1)
                  || this.measurementType.charAt(position2) == this.measurementType.charAt(position2 + 1)) {
                  this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'Enter valid Measurement Type.' });
              } else {
                  this.kraMeasurementTypeData.KRAMeasurementType = this.measurementType.trim();
                  this._kraMeasurementTypeService.CreateKraMeasurementType(this.kraMeasurementTypeData).subscribe(
                      (data: number) => {
                          if (data == 1) {
                              this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'KRA Measurement Type added successfully.' });
                            } else if (data == -1) {
                              this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'KRA Measurement Type already exists.' });
                          } else {
                              this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Failed to add KRA Measurement Type.' });
                          }
                          this.measurementFormSubmitted = false;
                          this.measurementType = "";
                          this.getMeasurementTypes();
                      },
                      (error: any) => {
                          this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Sorry! Failed to add KRA Measurement Type.' });
                      });
              }
          }
      }
  }

  private editMeasurement(kraMeasurementTypeId: number, kraMeasurementType: string) {
      this.buttonTitle = "Update";
      if (kraMeasurementTypeId && kraMeasurementType && kraMeasurementType != "") {
          this.kraMeasurementTypeData = new KRAMeasurementTypeData();
          this.kraMeasurementTypeData.KRAMeasurementTypeID = kraMeasurementTypeId;
          this.kraMeasurementTypeData.KRAMeasurementType = kraMeasurementType;
          this.measurementType = kraMeasurementType;
      }
  }

  private updateKraMeasurementType() {
      this.measurementFormSubmitted = true;
      if (
          this.kraMeasurementTypeData.KRAMeasurementTypeID && this.measurementType && this.measurementType != "") {
          let position: number = this.measurementType.indexOf('-', 0);
          let position1: number = this.measurementType.indexOf('&', 0);
          let position2: number = this.measurementType.indexOf(',', 0)
          if (this.measurementType.trim().length == 0
              || !this.valid || this.measurementType.charAt(position) == this.measurementType.charAt(position + 1) || this.measurementType.charAt(position1) == this.measurementType.charAt(position1 + 1)
              || this.measurementType.charAt(position2) == this.measurementType.charAt(position2 + 1)) {
              this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'Enter valid Measurement Type.' });
          } else {
              this.kraMeasurementTypeData.KRAMeasurementType = this.measurementType.trim();
              this._kraMeasurementTypeService.UpdateKraMeasurementType(this.kraMeasurementTypeData).subscribe(
                  (data: number) => {
                      if (data == 1) {

                          this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'KRA Measurement Type updated successfully.' });
                      } else if (data == -1) {
                          this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'KRA Measurement Type already exists.' });
                      } else if (data == -11) {
                          this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'Nothing to Save.' });
                      } else if (data == 9) {
                          this.messageService.add({ severity: 'warn', summary: 'Warning Message', detail: 'Child Dependency exists.' });
                      } else {
                          this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Failed to update KRA Measurement Type.' });
                      }
                      this.buttonTitle = "Add";
                      this.measurementFormSubmitted = false;
                      this.measurementType = "";
                      this.getMeasurementTypes();
                  },
                  (error: any) => {
                      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Sorry! Failed to update KRA Measurement Type.' });
                  });
          }
      }
  }

  private deleteMeasurement(kraMeasurementTypeId: number) {
      if (kraMeasurementTypeId != null) {
          this._confirmationService.confirm({
              message: 'Are you sure, you want to delete this Measurement Type?',
              header: 'Measurement Type Confirmation',
              key: 'kraMeasurementType',
              icon: 'fa fa-trash',
              accept: () => {
                  this._kraMeasurementTypeService.DeleteKraMeasurementType(kraMeasurementTypeId).subscribe(
                      (data: number) => {
                          if (data == 1) {
                              this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'KRA Measurement Type deleted successfully.' });
                          } else if (data == 0) {
                              this.messageService.add({ severity: 'warn', summary: 'Wraning Message', detail: 'Failed to delete KRA Measurement Type.' });
                          } else if (data == 9) {
                              this.messageService.add({ severity: 'warn', summary: 'Wraning Message', detail: 'Child Dependency exists.' });
                          } else {
                              this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Failed to delete KRA Measurement Type.' });
                          }
                          this.buttonTitle = "Add";
                          this.measurementFormSubmitted = false;
                          this.measurementType = "";
                          this.getMeasurementTypes();
                      },
                      (error: any) => {
                          this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Sorry! Failed to delete KRA Measurement Type.' });
                      });
              },
              reject: () => {
              }
          });
      }
  }

  public cancel(): void {
      this.buttonTitle = "Add";
      this.measurementForm.reset();
      this.measurementFormSubmitted = false;
  }
}
