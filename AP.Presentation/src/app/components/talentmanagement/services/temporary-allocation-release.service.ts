import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';
import { ResourceRelease } from '../models/resoucerelease.model';

@Injectable({
  providedIn: 'root'
})
export class TemporaryAllocationReleaseService {

  private _resources: any;
  private _serverURL: string;
  constructor(private _httpclient: HttpClient) {
      this._serverURL = environment.ServerUrl;
      this._resources = servicePath.API.TemporaryAllocationRelease;
  }
  public GetEmployeesAllocations() {
      let url = this._serverURL + this._resources.GetEmployeesAllocations;
      return this._httpclient.get(url);
  }
  public TemporaryReleaseAssociate(releaseResource: ResourceRelease) {
      let _url = this._serverURL + this._resources.TemporaryReleaseAssociate;
      return this._httpclient.post(_url, releaseResource); 
  }

  public GetAssociatesToRelease(employeeId: number, roleName: string) {
      let url = this._serverURL + this._resources.GetAssociatesToRelease + employeeId + "&roleName=" + roleName;
      return this._httpclient.get(url);
  }
  public GetAssociatePrimaryProject(employeeId: number){
    let url = this._serverURL + this._resources.GetAssociatePrimaryProject + employeeId;
    return this._httpclient.get(url);
  }
}



