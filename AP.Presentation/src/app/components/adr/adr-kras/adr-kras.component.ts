
import { Component, OnInit } from "@angular/core";
import { MasterDataService } from "../../../services/masterdata.service";
import {  SelectItem } from "primeng/components/common/api";
import * as servicePath from "../../../service-paths";
import { OrganizationKras, AssociateKras } from "../../../models/associate-kras.model";
import { GenericType } from "../../../models/dropdowntype.model";
import { MessageService } from "primeng/api";
import { DepartmentDetails } from "src/app/models/role.model";
import { DepartmentType } from "../../shared/utility/enums";
import { AssociateRoleMappingData } from "src/app/models/associaterolemappingdata.model";
import { ProjectDetails } from "src/app/models/projects.model";
import { KRAService } from "../../kra/Services/kra.service";
import { CustomKRAService } from "../../kra/Services/custom-kra.service";
import { AssociatekraService } from "../../kra/Services/associatekra.service";
import { RolemasterService } from "../../kra/Services/rolemaster.service";

@Component({
  selector: 'app-adr-kras',
  templateUrl: './adr-kras.component.html',
  styleUrls: ['./adr-kras.component.scss'],
  providers: [
    AssociatekraService,
    RolemasterService,
    MasterDataService,
    KRAService,
    CustomKRAService
  ]
})

export class AdrKrasComponent implements OnInit {
  resources = servicePath.API.PagingConfigValue;
  loggedinUserRole: string;
  loggedInEmployeeId: number;
  public PageSize: number;
  public PageDropDown: number[];
  public selectedEmployees: AssociateRoleMappingData[] = [];
  public overideExisting = 0;
  public KraHeading: string;
  public associateKRAList: AssociateKras;
  public currentfinancialYearId: number = 0;
  public financialYearId: number = 0;
  departmentHeadDepartmentId: number;
  public financialYearsList: SelectItem[] = [];
  public projectsList: SelectItem[] = [];
  public rolesList: SelectItem[] = [];
  public departmentList: SelectItem[] = [];
  public formSubmitted: boolean = false;
  public associateRoleMapping: AssociateRoleMappingData;
  public associatesList: AssociateRoleMappingData[] = [];
  private departmentType: number;
  public isNonDelivery: boolean = true;
  _selectedFinancialYearId: number;
  showProjectsDropDown: boolean = false;
  showDepartmentsDropDown: boolean = false;
  public associateKraView: boolean = false;
  public pdfStatus: boolean = false;
  public aspectsList = [];
  public rowspansList = [];
  public adrCyclesList = [];
  public associatesContributionsDisplay: boolean = false;
  public managerFeedbackDisplay: boolean = false;
  public titleForComments: string = "";
  public associateContributions: string = "";
  public managerFeedback: string = "";
  public associateRating: string = "";
  public managerRating: string = "";
  public kraStatus: string;

  constructor(
    private associateKraService: AssociatekraService,
    private _kraService: KRAService,
    private masterDataService: MasterDataService,
    private _customKraService: CustomKRAService,
    private messageService: MessageService
  ) {
    this.associateKRAList = new AssociateKras();
    this.associateKRAList.OrganizationKRAs = new Array<OrganizationKras>();
    // this.associateKRAList.CustomKRAs = new Array<CustomKras>();
    this.PageSize = this.resources.PageSize;
    this.PageDropDown = this.resources.PageDropDown;
  }

  ngOnInit() {
    this.loggedinUserRole = JSON.parse(
      sessionStorage["AssociatePortal_UserInformation"]
    ).roleName;
    this. loggedinUserRole = 'Associate';
    this.loggedInEmployeeId = JSON.parse(
      sessionStorage["AssociatePortal_UserInformation"]
    ).employeeId;
    this.Clear();
    this.departmentType = DepartmentType.Delivery;
    this.getFinancialYears();
    this.getLastThreeAdrCylces();
    this.getCurrentFinancialYear();
    this.departmentList = [];
    this.departmentList.push({ label: "Select Department", value: null });
    this.projectsList = [];
    this.projectsList.push({ label: "Select Project", value: null });

    if (
      this.loggedinUserRole.indexOf("HRM") != -1 ||
      this.loggedinUserRole.indexOf("HR Head") != -1
    ) {
      this.showProjectsDropDown = true;
      this.showDepartmentsDropDown = true;
      this.getDepartments();
    } else if (this.loggedinUserRole.indexOf("Delivery Head") != -1) {
      this.showProjectsDropDown = true;
      this.showDepartmentsDropDown = false;
      this.isNonDelivery = false;
      this.getProjects();
    } else if (this.loggedinUserRole.indexOf("Program Manager") != -1) {
      this.showProjectsDropDown = true;
      this.showDepartmentsDropDown = false;
      this.isNonDelivery = false;
      this.getEmployeesByDepartmentId(
        this.departmentType,
        this.financialYearId
      );
    } else if (this.loggedinUserRole.indexOf("Associate") != -1) {
      this.showProjectsDropDown = false;
      this.showDepartmentsDropDown = false;
    }
  }

  cols = [
    { field: "KRAAspectName", header: "KRA Aspect" },
    { field: "KRAAspectMetric", header: "Metric" },
    { field: "KRAAspectTarget", header: "Target" }
  ];

  tabs = [
    { tab: "Cycle1", header: "Cycle 1" },
    { tab: "Cycle1", header: "Cycle 2" },
    { tab: "Cycle1", header: "Cycle 3" },
  ]

  private getFinancialYears(): void {
    this.masterDataService.GetFinancialYears().subscribe(
      (yearsdata: GenericType[]) => {
        this.financialYearsList = [];
        this.financialYearsList.push({
          label: "Select Financial Year",
          value: null
        });
        this.rolesList = [];
        this.rolesList.push({ label: "Select Role", value: null });
        yearsdata.forEach((e: GenericType) => {
          this.financialYearsList.push({ label: e.Name, value: e.Id });
        });
      },
      error => {
        this.messageService.add({
          severity: "error",
          summary: "Error Message",
          detail: "Failed to get Finacial Year List"
        });
      }
    );
  }

  public getCurrentFinancialYear(): void {
    this._kraService.getCurrentFinancialYear().subscribe(
      (yearsdata: GenericType) => {
        if (yearsdata != null) {
          this.currentfinancialYearId = yearsdata.Id;
          if (this.loggedinUserRole.indexOf("Associate") != -1) {
            this.getAssociateKRAs(this.loggedInEmployeeId, ' ', this.currentfinancialYearId);
          }
        }
      },
      (error: any) => {
        this.messageService.add({
          severity: "error",
          detail: "Failed to get current financial year!",
          summary: "Error Message"
        });
      }
    );
  }

  private getLastThreeAdrCylces(): void {
    this.adrCyclesList = 
      [{FinancialYearId: 11, ADRCycleID: 1 }, {FinancialYearId: 10, ADRCycleID: 2 }, {FinancialYearId: 10, ADRCycleID: 3 }]
  }
 
  public canDisableCheckbox(empInfo) {
    if (empInfo.KRAGroupId == null) {
      return true;
    }
    false;
  }

  private getDepartments(): void {
    this.masterDataService.GetDepartments().subscribe(
      (res: DepartmentDetails[]) => {
        res.forEach((element: DepartmentDetails) => {
          this.departmentList.push({
            label: element.Description,
            value: element.DepartmentId
          });
        });
      },
      (error: any) => {
        this.messageService.add({
          severity: "error",
          summary: "Error Message",
          detail: "Failed to get Departments details."
        });
      }
    );
  }

  private getProjects(): void {
    this.masterDataService.GetProjectsList().subscribe(
      (res: ProjectDetails[]) => {
        this.projectsList = [];
        this.associateRoleMapping.ProjectId = null;
        this.projectsList.push({ label: "Select Project", value: null });
        res.forEach((element: ProjectDetails) => {
          this.projectsList.push({
            label: element.ProjectName,
            value: element.ProjectId
          });
        });
      },
      (error: any) => {
        this.messageService.add({
          severity: "error",
          summary: "Error Message",
          detail: "Failed to get Projects list."
        });
      }
    );
  }

  private getProjectsByEmployeeId(): void {
    if (Number(this.loggedInEmployeeId) > 0) {
      this._customKraService
        .GetProjectsByProgramManagerId(this.loggedInEmployeeId)
        .subscribe(
          (result: GenericType[]) => {
            this.projectsList = [];
            this.projectsList.push({ label: "Select Project", value: null });
            if (result.length > 0) {
              result.forEach((element: GenericType) => {
                this.projectsList.push({
                  label: element.Name,
                  value: element.Id
                });
              });
            }
          },
          (error: any) => {
            this.messageService.add({
              severity: "error",
              summary: "Failed to get projects.",
              detail: ""
            });
          }
        );
    } else {
      this.projectsList = [];
      this.projectsList.push({ label: "Select Project", value: null });
    }
  }

  public getAssociateKRAs(EmployeeId: number, AssociateName: string, FinancialYearId: number): void {
    // this.loggedinUserRole = 'Program Manager';
    this.selectedEmployees = [];
    this.aspectsList = [];
    this.rowspansList = [];
    this.KraHeading = " ";
    this.kraStatus = 'Approved';
    this.kraStatus = 'Submitted';
    this.kraStatus = 'Drafted';
    this.associateKRAList.OrganizationKRAs = [];
    if (FinancialYearId == null) {
      this.associateKRAList.OrganizationKRAs = [];
      // this.associateKRAList.CustomKRAs = [];
      return;
    }
    if (this.loggedinUserRole.indexOf("Associate") != -1) {
      EmployeeId = this.loggedInEmployeeId;
      this.associateKraView = false;
    } else {
      this.KraHeading = AssociateName + "'s KRAs";
      this.associateKraView = true;
    }
    // this.associateKRAList.OrganizationKRAs =
    //   [{ KRAAspectName: "Client Delivery", KRAAspectMetric: "Overall client satisfaction rating (Average annual rating across all the projects in the group)", KRAAspectTarget: ">= 4.5", KRAMeasurementType: "Rating", Operator: '>=', KRATargetValue: '4.5', KRATargetPeriod: 'Yearly' },
    //   { KRAAspectName: "Client Delivery", KRAAspectMetric: "Client retention (of the group) (Prevent delivery failures to ensure 100% client retention; Client decision to move away from SenecaGlobal due to change of business priorities and plans is not counted as client loss.)", KRAAspectTarget: "= 100%", KRAMeasurementType: " Percentage ", Operator: '=', KRATargetValue: '100', KRATargetPeriod: 'Yearly' },
    //   { KRAAspectName: "Innovation and Improvement ", KRAAspectMetric: "Creative ideas and improvements (Technology, Engineering, Process) implemented for client delivery and for organization development (Projects in the group achieve their new ideas and improvements target for the year) ", KRAAspectTarget: "= 100%", KRAMeasurementType: " Percentage ", Operator: '=', KRATargetValue: '100', KRATargetPeriod: 'Yearly' },
    //   { KRAAspectName: "Business Development", KRAAspectMetric: "Development of planned assets (white papers, technical papers, project case studies, prototypes, POC’s, client proposals (RFP responses), …) for business development", KRAAspectTarget: "= 100%", KRAMeasurementType: " Percentage ", Operator: '=', KRATargetValue: '100', KRATargetPeriod: 'Yearly' },
    //   { KRAAspectName: "Teaming", KRAAspectMetric: "Project teaming leadership effectiveness (direct and support project team(s) to be unified to generate team outcomes to meet their project goals successfully) (Scale: 1 - 5; 1 – Poor, 2 – Fair, 3 – Good, 4 – Very Good, 5 – Excellent)", KRAAspectTarget: ">= 3", KRAMeasurementType: "Rating", Operator: '>=', KRATargetValue: '3', KRATargetPeriod: 'Yearly' }];

    this.associateKraService
      .GetAssociateKRAs(EmployeeId, FinancialYearId)
      .subscribe(
        (kraResponse: AssociateKras) => {
          this.associateKRAList.OrganizationKRAs = kraResponse.OrganizationKRAs;
          // this.associateKRAList.CustomKRAs = kraResponse.CustomKRAs;

          // Logic to calculate the RowSpan and Number of records for each KRA Aspect
          this.rowspansList = [0];
          for (
            let i = 0;
            i < this.associateKRAList.OrganizationKRAs.length;
            i++
          ) {
            var KRAAspect = this.associateKRAList.OrganizationKRAs[i]
              .KRAAspectName;
            if (
              i > 0 &&
              this.associateKRAList.OrganizationKRAs[i].KRAAspectName !=
                this.associateKRAList.OrganizationKRAs[i - 1].KRAAspectName
            ) {
              this.rowspansList.push(i);
            }
            if (this.associateKRAList.OrganizationKRAs[i].KRAMeasurementType == "Percentage") 
            {
              var KRAAspectTarget =
                this.associateKRAList.OrganizationKRAs[i].Operator + " " + this.associateKRAList.OrganizationKRAs[i].KRATargetValue + 
                "% (" + this.associateKRAList.OrganizationKRAs[i].KRATargetPeriod + ")";
            } else {
              var KRAAspectTarget =
                this.associateKRAList.OrganizationKRAs[i].Operator + " " + this.associateKRAList.OrganizationKRAs[i].KRATargetValue +
                " (" + this.associateKRAList.OrganizationKRAs[i].KRATargetPeriod + ")";
            }
            this.aspectsList.push({
              KRAAspectName: this.associateKRAList.OrganizationKRAs[i].KRAAspectName,
              KRAAspectMetric: this.associateKRAList.OrganizationKRAs[i].KRAAspectMetric,
              KRAAspectTarget: KRAAspectTarget,
              AspectCount: this.associateKRAList.OrganizationKRAs.filter(obj => obj.KRAAspectName === KRAAspect).length
            });
          }
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Error Message",
            detail: "Failed to get Associate KRAs List"
          });
        }
      );
  }

  public getEmployeesByDepartmentId(
    departmentId: number,
    financialYearId: number
  ): void {
    this.selectedEmployees = [];
    if (departmentId == null) {
      this.isNonDelivery = true;
      this.associatesList = [];
      return;
    }
    this.associatesList = new Array<AssociateRoleMappingData>();
    if (departmentId == this.departmentType) {
      if (
        this.loggedinUserRole.indexOf("HRM") != -1 ||
        this.loggedinUserRole.indexOf("HR Head") != -1
      ) {
        this.getProjects();
      } else {
        this.getProjectsByEmployeeId();
      }
      this.isNonDelivery = false;
    } else {
      this.projectsList = [];
      this.projectsList.push({ label: "Select Project", value: null });
      this.associateRoleMapping.ProjectId = null;
      let projectId: number = null;
      this.getEmployeesByDepartmentIdAndProjectId(
        departmentId,
        projectId,
        financialYearId
      );
      this.isNonDelivery = true;
    }
  }

  public getEmployeesByDepartmentIdAndProjectId(
    departmentId: number,
    projectId: number,
    financialYearId: number
  ): void {
    this.formSubmitted = true;
    this.selectedEmployees = [];
    if (
      this.loggedinUserRole.indexOf("Program Manager") != -1 ||
      this.loggedinUserRole.indexOf("Delivery Head") != -1
    )
      departmentId = this.departmentType;
    if (financialYearId == null) {
      this.associatesList = [];
      return;
    }
    if (departmentId == null) {
      this.associatesList = [];
      return;
    }
    if (departmentId == this.departmentType && projectId == null) {
      this.associatesList = [];
      return;
    }
    this.associateKraService
      .GetEmployeesByDepartmentIdAndProjectId(
        departmentId,
        projectId,
        financialYearId
      )
      .subscribe(
        (res: AssociateRoleMappingData[]) => {
          this.associatesList = [];
          this.associatesList = res;
        },
        (error: any) => {
          this.messageService.add({
            severity: "error",
            summary: "Error Message",
            detail: "Failed to get Employees list."
          });
        }
      );
  }

  public Clear(): void {
    this.formSubmitted = false;
    this.financialYearId = null;
    this.selectedEmployees = new Array<AssociateRoleMappingData>();
    if (this.loggedinUserRole == "HRM" || this.loggedinUserRole == "HR Head")
      this.isNonDelivery = true;
    else this.isNonDelivery = false;
    this.associateRoleMapping = new AssociateRoleMappingData();
    this.associatesList = new Array<AssociateRoleMappingData>();
  }

  public close(): void {
    this.associateKraView = false;
  }

  setSelectedTab(event) {
    // this.associateKraService.SetSeletedTab(event.index);
    if (event.index == 0) {
      this.getAssociateKRAs(this.loggedInEmployeeId, ' ', 11);
    }
    else if (event.index == 1) {
      this.getAssociateKRAs(this.loggedInEmployeeId, ' ', 10);
    }
    else if (event.index == 2) {
      this.getAssociateKRAs(this.loggedInEmployeeId, ' ', 10);
    }
  } 

  public displayAssociateContributions(selectedData: OrganizationKras): void {
    this.associatesContributionsDisplay = true;
    this.titleForComments = 'Associate Contributions';
    this.associateContributions = '<ol><li><strong style="color: rgb(51, 51, 51);">Overall client satisfaction rating (Average annual rating across all the projects in the group)</strong></li><li><em style="color: rgb(51, 51, 51);">Overall client satisfaction rating (Average annual rating across all the projects in the group)</em></li><li><u>Overall client satisfaction rating (Average annual rating across all the projects in the group)</u></li></ol>';
    // this.associateContributions = selectedData.KRAAspectMetric;
    this.associateRating = selectedData.KRAAspectTarget;
  }

  public displayManagerFeedback(selectedData: OrganizationKras): void {
    this.managerFeedbackDisplay = true;
    this.titleForComments = 'Manager Feedback';
    this.managerFeedback = '<ol><li><strong style="color: rgb(51, 51, 51);">Overall client satisfaction rating (Average annual rating across all the projects in the group)</strong></li><li><em style="color: rgb(51, 51, 51);">Overall client satisfaction rating (Average annual rating across all the projects in the group)</em></li><li><u>Overall client satisfaction rating (Average annual rating across all the projects in the group)</u></li></ol>';
    // this.managerFeedback = selectedData.KRAAspectMetric;
    this.managerRating = selectedData.KRAAspectTarget;
  }
  
  public updateAssociateContributions(selectedData: OrganizationKras): void {
    this.associatesContributionsDisplay = false;
    this.messageService.add({ severity: 'success', detail: 'Data saved Successfully.', summary: 'Success Message' });
  }
  
  public updateManagerFeedback(selectedData: OrganizationKras): void {
    this.managerFeedbackDisplay = false;
    this.messageService.add({ severity: 'success', detail: 'Data saved Successfully.', summary: 'Success Message' });
  }

  public CancelComments() {
    this.associatesContributionsDisplay = false;
    this.managerFeedbackDisplay = false;
    this.associateContributions = null;
    this.managerFeedback = null;
  }

}
