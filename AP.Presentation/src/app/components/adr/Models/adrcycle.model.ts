export class ADRCycle{
    Id: number;
    FinancialYear: string;
    CycleName: string;
    FromMonth: number;
    ToMonth: number;
    IsActive: boolean;
}