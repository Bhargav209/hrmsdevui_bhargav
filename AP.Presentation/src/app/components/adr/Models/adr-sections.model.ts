export class ADRSections{
    ADRSectionId : number;
    ADRSectionName : string;
    ADRMeasurementAreaId : number;
    ADRMeasurementAreaName : string;
    DepartmentIds : string;
    DepartmentDescription : string;
}