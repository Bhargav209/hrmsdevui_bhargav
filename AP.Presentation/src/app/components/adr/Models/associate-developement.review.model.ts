export class ADRCycle {
    ADRCycleID?: number;
    ADRCycle?: string;
    IsActive?: boolean;
    checked?: boolean;
}
export class Appreciation extends ADRCycle {
    FinancialYearID?:number;
    FinancialYear?:string
    AppreciationDate?: string;
    ToEmployeeName?: string;
    SourceOfOriginID?: number;
    SourceOfOrigin?: string;
    ToEmployeeID?:number
    AssociateNames?: AppreciateDropDownType[];
    AppreciationTypeID?: number;
    AppreciationType?: string;
    AppreciationMessage?: string;
    FromEmployeeID?: number;
    FromEmployeeName?: string;
    ID?: number;
}
 export class AppreciateDropDownType {
     Id?:number;
     Name?:string;
 }
