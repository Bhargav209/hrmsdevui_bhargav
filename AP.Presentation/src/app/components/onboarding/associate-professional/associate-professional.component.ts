import { Component, OnInit } from '@angular/core';
import * as servicePath from '../../../service-paths';
import { MemberShip, Certification, Professional } from '../models/professionaldetails.model';
import { ProfessionalService } from '../services/professional.service';
import { CommonService } from '../../../services/common.service';
import { MasterDataService } from '../../../services/masterdata.service';
import { FormGroup } from '@angular/forms';
import { DropDownType, SkillGroupDropDown, SkillDropDown } from '../../../models/dropdowntype.model';
import { ViewChild } from '@angular/core';
import { Dialog } from 'primeng/components/dialog/dialog';
import { FormBuilder } from '@angular/forms';
import { ProgramType } from '../../shared/utility/enums';
import { Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-associate-professional',
  templateUrl: './associate-professional.component.html',
  styleUrls: ['./associate-professional.component.scss'],
  providers: [CommonService, MessageService]
})
export class AssociateProfessionalComponent implements OnInit {
  myForm: FormGroup;
    formSubmitted: boolean = false;
    employeeId: number;
    certificationList: any;
    memberShipList: any;
    professionalList: Professional[]
    professionalDetail: Professional;
    programType: DropDownType[];
    professionalType: number;
    deleteID: number;
    skillGroups: DropDownType[];
    skills: DropDownType[];
    certification: boolean = false;
    membership: boolean = false;
    addDisplay: boolean = false;
    inputDisable: boolean = false;
    deletedisplay: boolean = false;
    skillGroupID: number;
    date = new Date();
    upDateId: number;
    validYear: number;
    validFromErrorMessage: string = "";
    validUpToErrorMessage: string = "";
    @ViewChild('programTypeDialog') programTypeDialog: Dialog;
   PageSize: number;
   PageDropDown: number[] = [];
  private resources = servicePath.API.PagingConfigValue;
  
certificationCols = [
  {field: 'SkillGroupName', header: 'SkillGroupName' },
    {field : 'SkillName', header : 'SkillName'},
    {field : 'institution', header : 'Institution'},
    {field: 'specialization', header: 'Specialization' },
    {field : 'validFrom', header : 'ValidFrom'},
    {field : 'validUpto', header : 'ValidUpto'}
];
membershipCols = [
    {field: 'programTitle', header: 'MembershipTitle' },
  {field : 'institution', header : 'Institution'},
  {field: 'specialization', header: 'Specialization' },
  {field : 'validFrom', header : 'ValidFrom'},
  {field : 'validUpto', header : 'ValidUpto'}
];


  constructor(public fb: FormBuilder,private messageService: MessageService,private actRoute: ActivatedRoute, private _common : CommonService, private _professionalService: ProfessionalService, private masterDataService:MasterDataService) {
    this.programType = new Array<DropDownType>();
    this.skillGroups = new Array<DropDownType>();
    this.skills = new Array<DropDownType>();
    this.professionalList = new Array<Professional>();
    this.professionalDetail = new Professional();
     this.PageSize = this.resources.PageSize;
       this.PageDropDown = this.resources.PageDropDown;
}

  ngOnInit() {
    this.formSubmitted = false;
        this.actRoute.params.subscribe(params => { this.employeeId = params['id']; });
        this.skillGroupID = -1;
        this.professionalType = -1;
        this.upDateId = -1;
        this.deleteID = -1;
        this.deletedisplay = false;
        this.formSubmitted = false;
        this.inputDisable = false;
        this.certification = false;
        this.membership = false;
        this.addDisplay = false;
        this.programType.push({ label: 'Select ProgramType', value: null });
        this.programType.push({ label: 'Certificate', value: ProgramType.certification });
        this.programType.push({ label: 'Member Ship', value: ProgramType.membership });
        this.myForm = this.fb.group({
            'programType': [null, [Validators.required]],
            'skillGroupId': [null, null],
            'skillId': [null, null],
            'programTitle': [null, [Validators.pattern('[A-Za-z][a-zA-Z\\s]{2,64}')]],
            'specialization': [null, [Validators.required, Validators.pattern('[A-Za-z][a-zA-Z\\s]{2,64}')]],
            'institution': [null, [Validators.required, Validators.pattern('[A-Za-z][a-zA-Z\\s]{2,64}')]],
            'validFrom': [null, [Validators.required, Validators.pattern('(19|20)[0-9]{2}$')]],
            'validUpTo': [null, [Validators.required, Validators.pattern('(19|20)[0-9]{2}$')]]
        });
        this.getProfessionalDetail();
        this.skillGroups = [];
        this.skillGroups.push({ label: 'Select Skill Group', value: null });
        this.skills = [];
        this.skills.push({ label: 'Select Skill Name', value: null });
  }

   getProfessionalDetail() {
      this._professionalService.getProfessionalDetails(this.employeeId).subscribe((professionalList: Professional[]) => {
        this.fillCertifications(professionalList);
        this.fillMembership(professionalList);
    }, (error: any) => {
        this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to get Professional Details'});
    });
}
 fillCertifications(professionalList: Array<Professional>) {
    this.certificationList = professionalList.filter(x=>x.programType==ProgramType.certification);
    return this.certificationList;
}
 fillMembership(professionalList: Array<Professional>) {
    this.memberShipList = professionalList.filter(x=>x.programType==ProgramType.membership);
    return this.memberShipList;
}
 getSkillGroup() {
    this._professionalService.getSkillGroupCertificate().subscribe((skillGroupList: SkillGroupDropDown[]) => {
        this.skillGroups = [];
        this.skillGroups.push({ label: 'Select Skill Group', value: null });
        skillGroupList.forEach((e: SkillGroupDropDown) => {
            this.skillGroups.push({ label: e.SkillGroupName, value: e.SkillGroupId })
        });
    }, (error: any) => {
        this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to get Skill Group List.'});
    });
}
 skilGroupChange(event: any) {
    this.skillGroupID = -1;
    this.skillGroupID = event.value;
    this.myForm.controls['skillId'].setValue(null);
    this.getSkillNames()
}
 getSkillNames() {
    this.masterDataService.GetSkillsBySkillGroups(this.skillGroupID).subscribe((skillList: SkillDropDown[]) => {
        this.skills = [];
        this.skills.push({ label: 'Select Skill Name', value: null });
        skillList.forEach((e: SkillDropDown) => {
            this.skills.push({ label: e.SkillName, value: e.SkillId })
        });

    }, (error: any) => {
        this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to get Skill Group List.'});
    });
}
 addCertificationOrMembership() {
    this.getSkillGroup();
    this.getSkillNames();
    this.validFromErrorMessage = "";
    this.validUpToErrorMessage = "";
    this.myForm.reset();
    this.formSubmitted = false;
    this.inputDisable = false;
    this.professionalDetail.programType = null;
    this.membership = false;
    this.certification = false;
    this.addDisplay = true;
}
 programTypeChange(event: any) {
    this.validFromErrorMessage = "";
    this.validUpToErrorMessage = "";
    this.myForm.controls['skillGroupId'].setValue(null);
    this.myForm.controls['skillId'].setValue(null);
    this.myForm.controls['programTitle'].setValue(null);
    this.myForm.controls['institution'].setValue("");
    this.myForm.controls['specialization'].setValue("");
    this.myForm.controls['institution'].setValue("");
    this.myForm.controls['validFrom'].setValue(null);
    this.myForm.controls['validUpTo'].setValue(null);
    this.formSubmitted = false;
    if (event.value == ProgramType.certification) {
        this.certification = true;
        this.membership = false;
    }
    else if (event.value == ProgramType.membership) {
        this.membership = true;
        this.certification = false;
    }
    else {
        this.membership = false;
        this.certification = false;
    }
}
 getCertificationEdit(event: any) {
    this.skillGroupID = event.skillGroupID;
    this.getSkillGroup();
    this.getSkillNames();
    this.addDisplay = true;
    this.certification = true;
    this.membership = false;
    this.inputDisable = true;
    this.upDateId = event.ID;
    this.professionalDetail.programType = event.programType;
    this.professionalDetail.skillGroupID = event.skillGroupID;
    this.professionalDetail.certificationID = event.certificationID;
    this.professionalDetail.specialization = event.specialization;
    this.professionalDetail.institution = event.institution;
    this.professionalDetail.validFrom = event.validFrom;
    this.professionalDetail.validUpto = event.validUpto;
}
 getMembershipEdit(event: any) {
    this.addDisplay = true;
    this.membership = true;
    this.certification = false;
    this.inputDisable = true;
    this.upDateId = event.ID;
    this.professionalDetail.programType = event.programType;
    this.professionalDetail.programTitle = event.programTitle;
    this.professionalDetail.specialization = event.specialization;
    this.professionalDetail.institution = event.institution;
    this.professionalDetail.validFrom = event.validFrom;
    this.professionalDetail.validUpto = event.validUpto;
}
 deleteCertificateOrMembership(event: any) {
    this.deleteID = event.ID;
    this.professionalType = event.programType;
    this.deletedisplay = true;
}
 deleteProfessionalDetails() {
    this._professionalService.deleteProfessionalDetails(this.deleteID, this.professionalType).subscribe((data) => {
        this.messageService.add({severity:'success', summary: 'Success Message', detail:'Professional Details Deleted successfully.'});
        this.deletedisplay = false;
        this.getProfessionalDetail();
        this.formSubmitted = false;
        this.myForm.reset();
    }, (error) => {
        this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to Delete Professional Details.'});
    });
}
 cancelDeleteCancel() {
    this.deletedisplay = false;
}
 saveCertificationOrMembership() {
    this.formSubmitted = true;
    if (!this.myForm.valid) return;
    let currentYear = this.date.getFullYear();
    if (this.professionalDetail.validFrom > currentYear) return false;
    if (this.professionalDetail.validFrom > this.professionalDetail.validUpto) return false;
    switch (this.professionalDetail.programType) {
        case ProgramType.certification:
            let certDetail: Certification;
            certDetail = new Certification();
            certDetail.employeeID = this.employeeId;
            if (this.upDateId) {
                certDetail.id = this.upDateId;
            }
            certDetail.skillGroupID = this.myForm.controls['skillGroupId'].value;
            certDetail.certificationID = this.myForm.controls['skillId'].value;
            certDetail.programType = this.professionalDetail.programType;
            certDetail.specialization = this.professionalDetail.specialization;
            certDetail.institution = this.professionalDetail.institution;
            certDetail.validFrom = this.professionalDetail.validFrom;
            certDetail.validUpto = this.professionalDetail.validUpto;
            if (certDetail && (!certDetail.skillGroupID || !certDetail.certificationID))
                return;
            for (var i = 0; i < this.certificationList.length; i++) {
                if (certDetail.id == -1 && (this.certificationList[i].certificationID == certDetail.certificationID && this.certificationList[i].skillGroupID == certDetail.skillGroupID
                    && this.certificationList[i].programType == certDetail.programType && this.certificationList[i].institution.toLowerCase() == certDetail.institution.toLowerCase()
                    && this.certificationList[i].specialization.toLowerCase() == certDetail.specialization.toLowerCase() && this.certificationList[i].validFrom == certDetail.validFrom 
                    && this.certificationList[i].validUpto == certDetail.validUpto)) {
                    this.messageService.add({severity:'warn', summary: 'waning Message', detail:'Certification details already exists.'});
                    return;
                }
            }
            if (certDetail && certDetail.id == -1) {
                this._professionalService.addCertificationDetails(certDetail).subscribe((data) => {
                    this.messageService.add({severity:'success', summary: 'Success Message', detail:'Certification Details saved successfully.'});
                    this.getProfessionalDetail();
                    this.formSubmitted = false;
                    this.myForm.reset();
                }, (error) => {
                    this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to save Certification Details.'});
                });
            }
            if (certDetail && certDetail.id != -1) {
                this._professionalService.updateCertificationDetails(certDetail).subscribe((data) => {
                    this.messageService.add({severity:'success', summary: 'Success Message', detail:'Certification Details Updated successfully.'});
                    this.getProfessionalDetail();
                    this.formSubmitted = false;
                    this.myForm.reset();
                    this.upDateId = -1;
                }, (error) => {
                    this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to Updated Certification Details.'});
                });
            }
            break;
        case ProgramType.membership:
            let memDetails: MemberShip
            memDetails = new MemberShip();
            memDetails.employeeID = this.employeeId;
            if (this.upDateId) {
                memDetails.id = this.upDateId;
            }
            memDetails.programType = this.professionalDetail.programType;
            memDetails.programTitle = this.myForm.controls['programTitle'].value;
            memDetails.specialization = this.professionalDetail.specialization;
            memDetails.institution = this.professionalDetail.institution;
            memDetails.validFrom = this.professionalDetail.validFrom;
            memDetails.validUpto = this.professionalDetail.validUpto;
            if (memDetails && !memDetails.programTitle)
                return;
            for (var i = 0; i < this.memberShipList.length; i++) {
                if (memDetails.id == -1 && (this.memberShipList[i].programTitle == memDetails.programTitle && this.memberShipList[i].programType == memDetails.programType
                    && this.memberShipList[i].institution.toLowerCase() == memDetails.institution.toLowerCase()
                    && this.memberShipList[i].specialization.toLowerCase() == memDetails.specialization.toLowerCase() 
                    && this.memberShipList[i].validFrom == memDetails.validFrom && this.memberShipList[i].validUpto == memDetails.validUpto)) {
                    this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Membership details already exists.'});
                    return;
                }
            }
            if (memDetails && memDetails.id == -1) {
                this._professionalService.addMembershipDetails(memDetails).subscribe((data) => {
                    this.messageService.add({severity:'success', summary: 'Success Message', detail:'Membership Details saved successfully.'});
                    this.getProfessionalDetail();
                    this.formSubmitted = false;
                    this.myForm.reset();
                }, (error) => {
                    this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to save Membership Details.'});
                });
            }
            if (memDetails && memDetails.id != -1) {
                this._professionalService.updateMembershipDetails(memDetails).subscribe((data) => {
                    this.messageService.add({severity:'success', summary: 'Success Message', detail:'Membership Details Updated successfully.'});
                    this.getProfessionalDetail();
                    this.formSubmitted = false;
                    this.myForm.reset();
                    this.upDateId = -1;
                }, (error) => {
                    this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to Updated Membership Details.'});
                });
            }
            break;
        default:
            break;
    }
    this.addDisplay = false;
}
 cancelOrclearValues() {
    this.addDisplay = false;
    this.myForm.reset();
    this.formSubmitted = false;
}

 onlyNumbers(event: any) {
    this._common.onlyNumbers(event)
};
 validFromYear(event: any) {
    this.validFromErrorMessage = "";
    let currentYear = this.date.getFullYear();
    this.validYear = event.target.value;
    if (this.validYear > currentYear) {
        this.validFromErrorMessage = "Valid From must be less than current year";
        return false;
    }
}
 validUpToYear(event: any) {
    this.validUpToErrorMessage = "";
    if (this.validYear > event.target.value) {
        this.validUpToErrorMessage = "Valid Up To must be greater than Valid From";
        return false;
    }
}

}







