import { Component, OnInit, ViewChild } from '@angular/core';
import { EmergencyContactDetail, Relation } from '../models/family.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { FamilyService } from '../services/family.service';
import { Associate } from 'src/app/components/onboarding/models/associate.model';
import { ConfirmationService } from 'primeng/api';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-family-associate',
  templateUrl: './family-associate.component.html',
  styleUrls: ['./family-associate.component.scss'],
  providers: [MessageService, ConfirmationService, CommonService]
})
export class FamilyAssociateComponent implements OnInit {
  id: number;
  currentempID: number;
  isPrimary: boolean;
  _Associate = new Associate();
  relationsInfo: Array<Relation>;
  contactDetailsOne = new EmergencyContactDetail();
  contactDetailsTwo = new EmergencyContactDetail();
  selectedFamilyDetails: any;
  type: string = "new";
  valueKey: string = "Relation";
  valueKey1: string = "Country";
  ddlRelations: any[];
  ddlCountries: any[];
  birthDate: Date;
  shortYearCutoff: any = '+10';
  mobileErrorMessage: string;
  formSubmitted: boolean = false;
  firstcontactzipLength: number;
  secondcontactzipLength: number;
  rship;
  //isNew:boolean=true;
  @ViewChild('familyDialog') familyDialog: any;
  submitAttempt: boolean = false;
  @ViewChild('messageToaster') messageToaster: any;

  constructor(private messageService: MessageService, private _commonService: CommonService, private confirmationService: ConfirmationService, private _service: FamilyService, private router: Router, private actRoute: ActivatedRoute) { }

  ngOnInit() {
    this.actRoute.params.subscribe(params => { this.id = params['id']; });
    // this.id = 214;
    this.currentempID = this.id;
    this.getRelations(this.valueKey);
    this.getCountries(this.valueKey1);
    this.getFamilydetails(this.currentempID);
    this.getDates();
  }

  //   OpenConfirmationDialog() {   // method to open dialog
  //   this.confirmationService.confirm({
  //     message: 'Do you want to clear ?',
  //     accept: () => {
  //       this.onClear();
  //     },
  //     reject: () => {

  //     }
  //   });
  // }
  onClear() {
    this.formSubmitted = false;
    this._Associate.relationsInfo = new Array<Relation>();

    this._Associate.relationsInfo.push({
      birthDate: null,
      name: "",
      occupation: "",
      relationship: ""
    });
    this._Associate.contactDetailsOne = new EmergencyContactDetail();
    this._Associate.contactDetailsTwo = new EmergencyContactDetail();
    this._Associate.contactDetailsOne.isPrimary = false;
    this._Associate.contactDetailsTwo.isPrimary = false;
  }

  getRelations(valueKey: string) {
    this._commonService.GetBusinessValues(valueKey).subscribe((res: any) => {
      this.ddlRelations = res
    });
  }

  getCountries(valueKey1: string) {
    this._commonService.GetBusinessValues(valueKey1).subscribe((res: any) => {
      this.ddlCountries = res
    });
  }

  selectedDate(selectedDate: Date): Boolean {
    var today: any = new Date();
    today = today.getFullYear();
    selectedDate = moment(selectedDate).toDate();
    var selDate = selectedDate.getFullYear();
    var diff = Math.round(Math.abs(selDate - today));
    if (diff >= 100) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warn Message',
        detail: 'Please select a valid date'
      });

      return;
    }
  }

  getDates() {
    var date = new Date(),
      y = date.getFullYear(),
      m = date.getMonth(),
      d = date.getDate();
    this.birthDate = new Date(y, m, d - 1);
  }

  checkValidations = function () {
    this.submitAttempt = false;

    if (this._Associate.contactDetailsOne.isPrimary == true) {
      if (this._Associate.contactDetailsOne.contactName == undefined || this._Associate.contactDetailsOne.contactName == "" ||
        this._Associate.contactDetailsOne.relationship == undefined || this._Associate.contactDetailsOne.relationship == "" ||
        this._Associate.contactDetailsOne.addressLine1 == undefined || this._Associate.contactDetailsOne.addressLine1 == "" ||
        this._Associate.contactDetailsOne.city == undefined || this._Associate.contactDetailsOne.city == "" ||
        this._Associate.contactDetailsOne.state == undefined || this._Associate.contactDetailsOne.state == "" ||
        this._Associate.contactDetailsOne.zip == undefined || this._Associate.contactDetailsOne.zip == "" ||
        this._Associate.contactDetailsOne.country == undefined || this._Associate.contactDetailsOne.country == "" ||
        this._Associate.contactDetailsOne.mobileNo == undefined || this._Associate.contactDetailsOne.mobileNo == "")

        this.submitAttempt = true;

    }
    else if (this._Associate.contactDetailsTwo.isPrimary == true) {
      if (this._Associate.contactDetailsTwo.contactName == undefined || this._Associate.contactDetailsTwo.contactName == "" ||
        this._Associate.contactDetailsTwo.relationship == undefined || this._Associate.contactDetailsTwo.addressLine1 == undefined ||
        this._Associate.contactDetailsTwo.addressLine1 == "" || this._Associate.contactDetailsTwo.city == undefined ||
        this._Associate.contactDetailsTwo.city == "" || this._Associate.contactDetailsTwo.state == undefined ||
        this._Associate.contactDetailsTwo.state == "" || this._Associate.contactDetailsTwo.zip == undefined ||
        this._Associate.contactDetailsTwo.country == undefined || this._Associate.contactDetailsTwo.mobileNo == undefined ||
        this._Associate.contactDetailsTwo.contactName == "" || this._Associate.contactDetailsTwo.relationship == "" ||
        this._Associate.contactDetailsTwo.zip == null || this._Associate.contactDetailsTwo.country == "" ||
        this._Associate.contactDetailsTwo.mobileNo == "")

        this.submitAttempt = true;
    }
    if (this._Associate.contactDetailsOne.telephoneNo && this._Associate.contactDetailsOne.telephoneNo.length < 10)
      this.submitAttempt = true;
    if (this._Associate.contactDetailsOne.mobileNo && this._Associate.contactDetailsOne.mobileNo.length < 10)
      this.submitAttempt = true;
    if (this._Associate.contactDetailsTwo.telephoneNo && this._Associate.contactDetailsTwo.telephoneNo.length < 10)
      this.submitAttempt = true;
    if (this._Associate.contactDetailsTwo.mobileNo && this._Associate.contactDetailsTwo.mobileNo.length < 10)
      this.submitAttempt = true;
    if (this._Associate.contactDetailsOne.zip && this._Associate.contactDetailsOne.zip.length != this.firstcontactzipLength)
      this.submitAttempt = true;
    if (this._Associate.contactDetailsTwo.zip && this._Associate.contactDetailsTwo.zip.length != this.secondcontactzipLength)
      this.submitAttempt = true;

  }

  getFamilydetails = function (empID: number) {
    this._service.GetFamilyDetails(this.id).subscribe((res: any) => {

      if (res.relationsInfo.length != 0 || res.contactDetailsOne || res.contactDetailsTwo) {
        this.type = "edit";
        //this.isNew=false;
      }
      for (var i = 0; i < res.relationsInfo.length; i++) {
        res.relationsInfo[i].birthDate = res.relationsInfo[i].dob.replace("T00:00:00", '')
      }
      this._Associate.relationsInfo = res.relationsInfo;
      if (res.contactDetailsOne != null && res.contactDetailsOne != undefined) {
        this._Associate.contactDetailsOne = res.contactDetailsOne;
        if (this._Associate.contactDetailsOne.country == "India")
          this.firstcontactzipLength = 6;
        else this.firstcontactzipLength = 5;
      }
      if (res.contactDetailsTwo != null && res.contactDetailsTwo != undefined) {
        this._Associate.contactDetailsTwo = res.contactDetailsTwo;
        if (this._Associate.contactDetailsTwo.country == "India")
          this.secondcontactzipLength = 6;
        else this.secondcontactzipLength = 5;
      }
      if (this._Associate.relationsInfo.length == 0)
        this._Associate.relationsInfo.push({
          birthDate: null,
          name: "",
          occupation: "",
          relationship: ""
        });
      if (this._Associate.contactDetailsOne == null)
        this._Associate.contactDetailsOne = new EmergencyContactDetail();
      if (this._Associate.contactDetailsTwo == null)
        this._Associate.contactDetailsTwo = new EmergencyContactDetail();
    });
  }

  updateSelection(isprimaryname: string) {
    this.formSubmitted = false;
    if (isprimaryname == 'firstPrimary') {
      this._Associate.contactDetailsTwo.isPrimary = false;
      this._Associate.contactDetailsOne.isPrimary = true;
    }
    else if (isprimaryname == 'secondPrimary') {
      this._Associate.contactDetailsTwo.isPrimary = true;
      this._Associate.contactDetailsOne.isPrimary = false;
    }
  };

  onNewRelation(rShip: Relation) {
    this._Associate.relationsInfo.push({
      ID: 0,
      birthDate: null,
      name: "",
      occupation: "",
      relationship: ""
    });
  }

  onRelationChange(event: any, index: number) {
    let relationShip = event.target.value;
    let count = 0;
    if (relationShip) {

      let i, j: number;
      i = 0, j = 1;

      // do {
      //   let relationshipName = this._Associate.relationsInfo[i].relationship;
      //   if (relationshipName == "Mother" || relationshipName == "Father" || relationshipName == "Spouse" ||
      //     relationshipName == "Mother-in-Law" || relationshipName == "Father-in-Law") {
      //     j = i + 1;
      //     while (j < this._Associate.relationsInfo.length + 1) {
      //       if (this._Associate.relationsInfo[j].relationship &&
      //         (relationshipName == this._Associate.relationsInfo[j].relationship)) {
      //         event.target.value = "";
      //         this.messageService.add({ severity: 'warn', summary: 'Warn Message', detail: 'You cant select same relationship twice' });
      //         return;
      //       }
      //       j++;
      //     }
      //   }
      //   else i++;
      // } while (i < this._Associate.relationsInfo.length)
      
        if (relationShip == "Mother" || relationShip == "Father" || relationShip == "Spouse" ||
          relationShip == "Mother-in-Law" || relationShip == "Father-in-Law") {
          for (i = 0; i < this._Associate.relationsInfo.length; i++) {
            if (i != index) {
              let relationshipName = this._Associate.relationsInfo[i].relationship;
              if (relationShip == relationshipName) {
                event.target.value = "";
                this._Associate.relationsInfo[index].relationship = "";
                this.messageService.add({ severity: 'warn', summary: 'Warn Message', detail: 'You cant select same relationship twice' });
                return;
              }
            }
          }
        }
    }
  }

  onSaveorUpdate(emp: Associate) {
    this.formSubmitted = true;
    for (var i = 0; i < this._Associate.relationsInfo.length; i++) {
      if (this._Associate.relationsInfo[i].name == "" || this._Associate.relationsInfo[i].name.trim().length == 0 ||
        this._Associate.relationsInfo[i].relationship == "" || this._Associate.relationsInfo[i].relationship == null || this._Associate.relationsInfo[i].occupation == "" ||
        this._Associate.relationsInfo[i].occupation.trim().length == 0 || this._Associate.relationsInfo[i].birthDate == null) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warn Message',
          detail: 'Please enter relationship details'
        });
        return
      }
      else
        this._Associate.relationsInfo[i].dob = this._Associate.relationsInfo[i].birthDate;
    }
    this.checkValidations();
    this.isPrimary = false;
    if (this.submitAttempt == true) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warn Message',
        detail: 'Please complete contact details'
      });

      return
    }
    if (this.selectedDate(this._Associate.dob) == false)
      return;
    if (this._Associate.contactDetailsOne.emailAddress) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var isValid = re.test(this._Associate.contactDetailsOne.emailAddress);
      if (!isValid) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warn Message',
          detail: 'Please enter valid email format'
        });

        return
      }
    }
    if (this._Associate.contactDetailsTwo.emailAddress) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var isValid = re.test(this._Associate.contactDetailsTwo.emailAddress);

      if (!isValid) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warn Message',
          detail: 'Please enter valid email format'
        });

        return
      }
    }
    if (this._Associate.contactDetailsOne.isPrimary == true) {
      this.isPrimary = true;
    }
    if (this._Associate.contactDetailsTwo.isPrimary == true) {
      this.isPrimary = true;
    }
    if (this.isPrimary == false) {
      this.messageService.add({ severity: 'warn', summary: 'Warn Message', detail: 'Please select a primary contact' });
      return;
    }
    this._Associate.empID = this.currentempID;
    this._service.SaveFamilyDetails(this._Associate).subscribe((data) => {
      this.submitAttempt = false;
      this.messageService.add({
        severity: 'success',
        summary: 'Success Message',
        detail: 'Family details saved successfully'
      });

      this.onClear();
      this.getFamilydetails(this.currentempID);
    }, (error) => {
      this.messageService.add({
        severity: 'error',
        summary: 'Error Message',
        detail: 'Failed to save family details'
      });

    });

  }
  OpenConfirmationDialog(editMode: number) {   // method to open dialog
    this.confirmationService.confirm({
      message: 'Do you want to Delete ?',
      accept: () => {
        this.Delete();
      },
      reject: () => {

      }
    });
  }
  onDelete(selectedRelationship: any) {
    this.OpenConfirmationDialog(0);
    //  this.familyDialog.nativeElement.open();
    this.selectedFamilyDetails = selectedRelationship;
  }

  Delete() {
    this._Associate.relationsInfo.splice(this._Associate.relationsInfo.indexOf(this.selectedFamilyDetails), 1);
    //this.familyDialog.nativeElement.close();
  }

  onCancel() {
    this.familyDialog.nativeElement.close();
  }

  onlyNumbers(event: any) {
    this._commonService.onlyNumbers(event);
  }

  onlychar(event: any) {
    let k: any;
    k = event.charCode;
    // k = event.keyCode; (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32);
  }

  forAddress(event: any) {
    let k: any;
    k = event.charCode;
    // k = event.keyCode; (Both can be used)
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 92 || k == 35 || (k >= 44 && k <= 57));
  }

  onlyForNumbers(event: any) {
    var keys = { 'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57 };
    for (var index in keys) {
      if (!keys.hasOwnProperty(index))
        continue;
      if (event.charCode == keys[index] || event.keyCode == keys[index]) {
        return; //default event
      }
    }
    event.preventDefault();
  };

  phoneNumbers(event: any) {
    var keys = { 'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, '-': 45, '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57 };
    for (var index in keys) {
      if (!keys.hasOwnProperty(index))
        continue;
      if (event.charCode == keys[index] || event.keyCode == keys[index]) {
        return; //default event
      }
    }
    event.preventDefault();
  };

  setfirstcontactzipLength(event: any) {
    if (event.target.value == "India")
      this.firstcontactzipLength = 6;
    else this.firstcontactzipLength = 5;
  }

  setsecondcontactzipLength(event: any) {
    if (event.target.value == "India")
      this.secondcontactzipLength = 6;
    else this.secondcontactzipLength = 5;
  }

}