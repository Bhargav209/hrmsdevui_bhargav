import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProspectiveAssosiateService } from '../services/prospective-assosiate.service';
import * as servicePath from '../../../service-paths';
import * as moment from 'moment';
import { Associate } from '../models/associate.model';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-prospective-associate',
  templateUrl: './prospective-associate.component.html',
  styleUrls: ['./prospective-associate.component.scss'],
  providers: [ProspectiveAssosiateService]
})
export class ProspectiveAssociateComponent implements OnInit {
//   prosAssociatesList: any[];selectedTRs;
items: MenuItem[];

    home: MenuItem;
prosAssociatesList: any[];
  private resources = servicePath.API.PagingConfigValue;
   PageSize: number;
   PageDropDown: number[] = [];

    constructor(
      private _ProspectiveAssosiateService: ProspectiveAssosiateService,  private _router: Router
  )
 {
      this.PageSize = this.resources.PageSize;
      this.PageDropDown = this.resources.PageDropDown;
    }

    ngOnInit() {
        this.getprosAssociatesDetails();
        
    }
        cols = [
            { field: "name", header: "Name" },
            { field: "designation", header: "Designation" },
            { field: "department", header: "Department" },
            { field: "joiningDate", header: "Joining Date" },
            { field: "hrAdvisor", header: "Advisor" }
          ];
        
  

    getprosAssociatesDetails() {
        this._ProspectiveAssosiateService.list().subscribe((res: any) => { this.prosAssociatesList = res; 
             this.prosAssociatesList.forEach((r:any) => {
                 
                 r.joiningDate= moment(r.joiningDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
            });
        });
    }

    onEdit(selectedData: any) {        
            this._router.navigate(['/associates/editprospectiveassociate/' + selectedData.ID]);
    }

    onAdd() {
       this._router.navigate(['/associates/addprospectiveassociate']);
    }
    onConfirm(currentAssociate: Associate) {
        let subType = "profile";
        currentAssociate.associateType = currentAssociate.empID != 0 ? "edit" : "new";
        currentAssociate.ID = currentAssociate.empID == 0 ? currentAssociate.ID : currentAssociate.empID;
        this._router.navigate(['/associates/prospectivetoassociate/' + currentAssociate.associateType + '/' + currentAssociate.ID + '/' + subType]);
    }

}