import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';

@Injectable({
  providedIn: 'root'
})
export class AssociatejoiningService {
  private _serviceUrl = environment.ServerUrl;
  private resources = servicePath.API.AssociateJoining;
  constructor(private httpClient : HttpClient ) { }

  public getAssociates(){
    return this.httpClient.get(this._serviceUrl + this.resources.getJoinedAssociates)
  }

}
