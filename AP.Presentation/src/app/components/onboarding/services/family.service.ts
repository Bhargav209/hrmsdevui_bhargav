import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Associate } from '../models/associate.model';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';
  
  @Injectable({  
    providedIn: 'root'
  })
  
export class FamilyService 
{  
  private _resources: any;
  private _serverURL: string;
  
  constructor(private _http: HttpClient) { this._serverURL = environment.ServerUrl; this._resources = servicePath.API.family;}
   
  public GetFamilyDetails(id: number) 
  {
   var url = this._serverURL + this._resources.list + id;
   return this._http.get(url)
  }
  SaveFamilyDetails(details: Associate) 
  {  
   let _url = this._serverURL + this._resources.save;  
   return this._http.post(_url, details);
  
  }
  public DeleteFamilyDetails(familyDetailsID: number) 
  {
   let _url = this._serverURL + this._resources.deleteFamilyDetails + familyDetailsID;
   return this._http.post(_url, familyDetailsID ); 
  
  }
  
}
  
  
  
