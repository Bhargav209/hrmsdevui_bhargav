import {  Injectable } from '@angular/core';
import {HttpClient } from'@angular/common/http';
import * as servicePath from '../../../service-paths';
import {environment } from '../../../../environments/environment';
import { Associate } from '../models/associate.model';

@Injectable({
  providedIn: 'root'
})
export class ProspectiveassociateService {
  private _roleName: string;
  private _hrm: string;
  private _resources:any;
  private _serverURL = environment.ServerUrl;

  public notificationCount: number;
  constructor(private httpClient:HttpClient) {
      this._resources = servicePath.API.PAssociate;
  }  
  getPADetailsById(id:number) {        
      let _url = this._serverURL + this._resources.get+id;
      return this.httpClient.get(_url);         
  }
  UpdatePADetails(newAssociate:Associate) {        
      let _url = this._serverURL + this._resources.update;

        return this.httpClient.post(_url, newAssociate);
          
  }
  
}

