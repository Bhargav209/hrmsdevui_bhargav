import { Injectable,Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';
import { Associate } from '../models/associate.model';

@Injectable({
  providedIn: 'root'
})
export class EducationService {
  private _resources: any;
  private _serverURL = environment.ServerUrl;
  constructor( @Inject(HttpClient) private _httpClient: HttpClient) {
    this._resources = servicePath.API.education;
}


SaveEducationDetails(details: Associate)
{ 
    let url = this._serverURL + this._resources.save;
     return this._httpClient.post(url,details) 
  }


  public GetQualifications(empID : number){
    var url = this._serverURL + this._resources.list+"?empID="+empID;
    return this._httpClient.get(url)
}

}
