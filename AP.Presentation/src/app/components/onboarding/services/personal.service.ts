import { Injectable } from '@angular/core';
import { Associate } from '../models/associate.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as servicePath from '../../../service-paths';
import { environment } from '../../../../environments/environment';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  private _resources: any;
  private _serviceUrl = environment.ServerUrl;
  constructor(  private httpClient: HttpClient) {
      this._resources = servicePath.API.personal;
  }

  public GetPersonalDetails(id: number): Observable<Associate> {
      var url = this._serviceUrl + this._resources.getByPAId + id;
      return this.httpClient.get<Associate>(url);
      // .catch((err) => Util.handleError(err));
  }

  public GetEmployeePersonalDetails(id: number): Observable<Associate> {
      var url = this._serviceUrl + this._resources.getByEmpId + id;
      return this.httpClient.get<Associate>(url);
      // .catch((err) => Util.handleError(err));
  }

  public GetManagersAndLeads() {
      var url = this._serviceUrl + this._resources.getManagersAndLeads;
      return this.httpClient.get(url);
      // .catch((err) => Util.handleError(err));
  }


  public GetDesignations(gradeID: number) {
      var url = this._serviceUrl + this._resources.getDesignations + gradeID;
      return this.httpClient.get(url);
      // .catch((err) => Util.handleError(err));
  }

  public GetGradesDetails() {
      var url = this._serviceUrl + this._resources.getGradesDetails;
      return this.httpClient.get(url);
      // .catch((err) => Util.handleError(err));
  }

  public GetHRAdvisors() {
      var url = this._serviceUrl + this._resources.getHRAdvisors;
      return this.httpClient.get(url);
      // .catch((err) => Util.handleError(err));
  }

  public GetEmpTypes() {
      var url = this._serviceUrl + this._resources.getEmpTypes;
      return this.httpClient.get(url);
      // .catch((err) => Util.handleError(err));
  }

  public GetTechnologies() {
      var url = this._serviceUrl + this._resources.getTechnologies;
      return this.httpClient.get(url);
      // .catch((err) => Util.handleError(err));
  }

  SavePersonalDetails(details: Associate) {
      let _url = this._serviceUrl + this._resources.save;
      return this.httpClient.post(_url, details );
              // .map(res => res.json())
              // .subscribe((lData) => {
              //     resolve(lData);
              // }, error => reject(error));
     
  }

  UpdatePersonalDetails(details: Associate) {
      let _url = this._serviceUrl + this._resources.update;
      return this.httpClient.post(_url, details);
           
  }
}


