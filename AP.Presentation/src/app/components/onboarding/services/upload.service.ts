import { Injectable, Inject } from '@angular/core';
import {  Certification } from '../models/professionaldetail.model';
import { HttpClient } from '@angular/common/http';
import * as servicePath from '../../../service-paths';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private _resources: any;
  private _serviceUrl = environment.ServerUrl;
  constructor(@Inject(HttpClient) private httpClient: HttpClient) {  
    this._resources = servicePath.API.upload;
  }
public GetUploadData(id: number): Observable<Certification> {
      var url = this._serviceUrl + this._resources.list +id;
      return this.httpClient.get<Certification>(url);      
  }  

public GetPAstatus(id: number) {
    var url = this._serviceUrl + this._resources.GetPAstatus +id;
    return this.httpClient.get(url);      
}  


public SubmitForApproval(id: number) {
    var url = this._serviceUrl + this._resources.submitForApproval +id;
    return this.httpClient.post(url, id);      
}  
public Delete(employeeId: number,id: number) {
  var url = this._serviceUrl + this._resources.delete + '?empID=' + employeeId + "&&id=" + id;
  return this.httpClient.get(url);      
} 
}

