
// import { Qualification } from './education.model'
// import { AssociateSkill } from './associate-skills.model'

export class Associate {
    public ID: number;
    public KRARoleId: number;
    public empCode: string;
    public empID: number;
    public Year: number;
    public roleId: number;
    get name(): string {
        return this.firstName + " " + this.middleName + " " + this.lastName;
    }
    public emailAddress: string;
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public gender: string;
    public personalEmail: string;
    public dateOfJoining: Date;
    public dob: Date;
    public doj: Date;
    public Birthdate: string;
    public dateOfBirth: string;
    public maritalStatus: string;
    public bloodGroup: string;
    public nationality: string;
    public panNumber: string;
    public passportNumber: string;
    public passportIssuingOffice: string;
    public aadharNumber: string;
    public uanNumber: string;
    public bgvStatus: string;
    public bgvStartDate: Date;
    public bgvCompletedDate: Date
    public bgvStrtDate: string;
    public bgvCompletionDate: string
    public pfNumber: string;
    public joiningStatusID: number;
    public gradeID: any;
    public GradeName: string;
    public designationID: number;
    public designation: string;
    public employmentType: string;
    public technology: string;
    public technologyID: any;
    public deptID: any;
    public department: string;
    public hrAdvisor: string;
    public managerId: any;
    public ReportingManagerId: number;
    public joiningDate: string;
    public dropoutReason: string;
    public EmploymentStartDate:string;
    public EmplStartDate:Date;
    public CareerBreak:number;
    public Qualifications: any[];
    public prevEmployerdetails: any[];
    public profReference: any[];
    public certifications: any[];
    public relationsInfo: any[];
    public memberships: any[];
    public skills: any[];
    public Projects: any[];
    public contactDetails: any[];
    public contacts: any;
    public contactDetailsOne: any;
    public contactDetailsTwo: any;
    public contactDetailsThird: any;
    public RecruitedBy: string;
    public mobileNo: number;
    public phoneNumber: number;
    public associateType: string;
    public RoleSkillList: any[];
    public Experience: string;
    public EmployeeId: number;
    public passportValidDate: Date;
}

export class ContactDetails {
    public addressType: string;
    public currentAddress1: string;
    public currentAddress2: string;
    public currentAddCity: string;
    public currentAddState: string;
    public currentAddZip: string;
    public currentAddCountry: string;
    public permanentAddress1: string;
    public permanentAddress2: string;
    public permanentAddCity: string;
    public permanentAddState: string;
    public permanentAddZip: string;
    public permanentAddCountry: string;
    // public address : string;
    public address: any;
}