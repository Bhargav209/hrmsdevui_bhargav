export interface Projects{
    ID :number;
   empID?: number;
   organizationName:string;
   projectName:string;
   duration:number;
   roleID:any;
   roleName:string;
   keyAchievement:string;
   DomainID:number;
   DomainName?:string;
   }