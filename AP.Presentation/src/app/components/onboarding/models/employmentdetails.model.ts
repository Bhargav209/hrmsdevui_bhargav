export interface EmploymentDetails  {
    empID?: number;
    ID?: number;
    name: string;
    address: string;
    designation: string;
    serviceFrom: Date;
    serviceTo: Date;
    leavingReson: string;
    lastDrawnSalary?: number;
    fromYear?: string;
    toYear?: string
} 

export interface ProfessionalReferences {
    empID?: number;
    ID?: number;
    name: string;
    companyName: string;
    designation: string;
    mobileNo: string;
    companyAddress: string;
    lastDrawnSalary?: number;
    officeEmailAddress: string
} 


      