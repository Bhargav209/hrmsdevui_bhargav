export interface Relation {
    name: string;
    relationship: string;
    dob?: Date;
    birthDate: string;
    occupation: string;
}

export class EmergencyContactDetail {
    isPrimary: boolean;
    empID: number;
    contactName: string;
    contactType: string;
    birthDate: Date;
    occupation: string;
    relationship: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    country: string;
    state: string;
    zip: number;
    telephoneNo: number;
    MobileNo: number;
    emailAddress:string;
}