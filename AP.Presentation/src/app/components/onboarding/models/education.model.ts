export interface Qualification
{
    empID?:number;
    ID?: number;
    qualificationName: string;    
    yearCompleted: Date;
    institution: string;
    specialization: string;
    programTypeID: number;
    grade?: string;
    marks: number;   
   completedYear?: any;

}