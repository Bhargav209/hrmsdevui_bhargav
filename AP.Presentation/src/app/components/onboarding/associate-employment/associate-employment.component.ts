import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Qualification } from '../models/education.model';
import { Associate } from '../models/associate.model';
import { Inject } from '@angular/core';
import { Http } from '@angular/http';
import { EducationService } from '../services/education.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Injector } from '@angular/core';
import { AppInjector } from '../../shared/injector';
import * as moment from 'moment';
import { CommonService } from '../../../services/common.service';
import { ProfessionalReferences, EmploymentDetails } from '../models/employmentdetails.model';
import { EmploymentService } from '../services/employment.service';
import {MessageService} from 'primeng/api';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';


@Component({
  selector: 'app-associate-employment',
  templateUrl: './associate-employment.component.html',
  styleUrls: ['./associate-employment.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class AssociateEmploymentComponent implements OnInit {
    id: number;
    _Associate = new Associate();
    currentempID: number;
    @ViewChild('messageToaster') messageToaster: any;
    @ViewChild('PrevEmploymentDialog') PrevEmploymentDialog: any;
    @ViewChild('profDialog') profDialog: any;
    private _serverURL: string;
    type: string = 'new';
    lastDate: Date;
    index: number;
    yearRange: string;
    emp:any;
    constructor( private _injector: Injector = AppInjector(),
    private _dialogservice : ConfirmationService,
        @Inject(EmploymentService) private _service: EmploymentService,
        private messageService: MessageService,
        @Inject(Router) private _router: Router, private actRoute: ActivatedRoute) {
        this._Associate.prevEmployerdetails = new Array<EmploymentDetails>();
        this._Associate.prevEmployerdetails.push({ serviceFrom: null, serviceTo: null, name: "", designation: "", address: "", leavingReson: "" });
        this._Associate.profReference = new Array<ProfessionalReferences>();
        this._Associate.profReference.push({ name: null, companyName: null, designation: "", mobileNo: "", companyAddress: "", officeEmailAddress: "" });
    }

    ngOnInit() {
        this.actRoute.params.subscribe(params => { this.id = params['id']; });
        this.currentempID = this.id;
        this.yearRange  = (new Date().getFullYear() - 50) + ':' + (new Date().getFullYear() + 0);
        this.getEmploymentdetails(this.currentempID);
        this.getDates();
    }

    forAddress(event: any){
        let k: any;  
        k = event.charCode;  //         k = event.keyCode;  (Both can be used)
        return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 92 ||  k == 35 || (k >= 44 && k <= 57));
    }

    omit_special_char(event: any)
    {   
       let k: any;  
       k = event.charCode;  //         k = event.keyCode;  (Both can be used)
       return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 ||  k == 46 || k == 44 || (k >= 48 && k <= 57));
    }

    onlychar(event: any)
    {   
       let k: any;  
       k = event.charCode;  //         k = event.keyCode;  (Both can be used)
       return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32);
    }

    onNewEmployment(empdetails: EmploymentDetails) {
        this._Associate.prevEmployerdetails.push({ serviceFrom: null, serviceTo: null, name: "", designation: "", address: "", leavingReson: "" });
    }

    onDeleteEmployment(index: number) {
        this.index = index;
        this.OpenConfirmationDialog("employement"); 
        
    }

    DeleteEmployment() {
        this._Associate.prevEmployerdetails.splice(this.index, 1);
        this.PrevEmploymentDialog.nativeElement.close();
    }
    OpenConfirmationDialog(type:string) {   // method to open dialog
        this._dialogservice.confirm({
            message: 'Are you sure, you want to delete this?',
            accept: () => {
            if(type=="employement")
            this.DeleteEmployment();  
            else
            this.Delete();
            } ,
            reject : ()=>{
                this.Cancel();
            
            }
        });
    }

    Cancel() {
        this.PrevEmploymentDialog.nativeElement.close();
    }
    onNewProfRef(profdetails: ProfessionalReferences) {
        this._Associate.profReference.push({ name: null, companyName: null, designation: "", mobileNo: "", companyAddress: "", officeEmailAddress: "" });
    }

    onDeleteProfessionalRef(index: number) {
        // this.profDialog.nativeElement.open();
         this.index = index;
        this.OpenConfirmationDialog("Professinal"); 
        
    }

    Delete() {
        this._Associate.profReference.splice(this.index, 1);
        this.profDialog.nativeElement.close();
    }

    onCancel() {
        this.profDialog.nativeElement.close();
    }

    getDates() {
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDate();
        this.lastDate = new Date(y, m, d);
    }

    validate(toYear: Date, fromYear: Date): number  {
        let count = 0;
        if(toYear && fromYear) {
        this._Associate.prevEmployerdetails.forEach((details: any) => {
           if (details.toYear == toYear && details.fromYear == fromYear)
                count++;
        });
    }
        if (count == 2) {
            this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'You cant select same service duration'});
        }
        return count;
    }

    getEmploymentdetails = function (empID: number) {
        this._Associate.empID=empID;
        this._service.GetEmploymentDetails(this.id).subscribe((res: any) => {
            for (var i = 0; i < res.length; i++) {
                res[i].fromYear = res[i].serviceFrom.replace("T00:00:00", '')
                res[i].toYear = res[i].serviceTo.replace("T00:00:00", '')
            }
            this._Associate.prevEmployerdetails = res;

            if (this._Associate.prevEmployerdetails.length == 0)
                this._Associate.prevEmployerdetails.push({ serviceFrom: null, serviceTo: null, name: "", designation: "", address: "", leavingReson: "" });
        });
        this._service.GetProfReferenceDetails(this.id).subscribe((res: any) => {
            this._Associate.profReference = res;
            if (this._Associate.profReference.length != 0) {
                this.type = "edit";
            }
            if (this._Associate.profReference.length == 0)
                this._Associate.profReference.push({ name: null, companyName: null, designation: "", mobileNo: "", companyAddress: "", officeEmailAddress: "" });
        });
    };


    IsValidDate = function (fromDate: any, toDate: any) {
        if (Date.parse(fromDate) <= Date.parse(toDate))
            return true;
        return false;
    }

    ValidatePrevEmployerdetails = function () {
        var prevEmployerdetails = this._Associate.prevEmployerdetails;
        if (prevEmployerdetails.length == 1 && !prevEmployerdetails[0].name)
            return 3;
        for (var count = 0; count < prevEmployerdetails.length; count++) {

            if (!prevEmployerdetails[count].name || prevEmployerdetails[count].name.trim().length ==0 
            || !prevEmployerdetails[count].address || prevEmployerdetails[count].address.trim().length ==0 
            || !prevEmployerdetails[count].designation || prevEmployerdetails[count].designation.trim().length ==0 
            || !prevEmployerdetails[count].fromYear 
            || !prevEmployerdetails[count].toYear) {

                if (count == (prevEmployerdetails.length - 1) && !prevEmployerdetails[count].name) {
                    return 3;
                }

                return 3;
            }
            if (!this.IsValidDate(prevEmployerdetails[count].fromYear, prevEmployerdetails[count].toYear)) {
                return 2;
            }
        };
        return 1;
    }

    ValidateProfReference = function () {
        var profReference = this._Associate.profReference;
        if (profReference.length == 1 && !profReference[0].name)
            return false;
        for (var count = 0; count < profReference.length; count++) {
            if (!profReference[count].name || profReference[count].name.trim().length == 0
                || !profReference[count].designation || profReference[count].name.trim().designation == 0
                || !profReference[count].companyName || profReference[count].name.trim().companyName == 0
                || !profReference[count].companyAddress || profReference[count].name.trim().companyAddress == 0
                || !profReference[count].officeEmailAddress || profReference[count].name.trim().officeEmailAddress == 0
                || !profReference[count].mobileNo) {
                if (count == (profReference.length - 1) && !profReference[count].name) {
                    return false;
                }
                return false;
            }
        };
        return true;
    }

    
    
    onSaveorUpdate(emp: Associate) {
        
        var today: any = new Date();
        today = today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
        var IsValidPrevEmployerdetail = this.ValidatePrevEmployerdetails();

        if (IsValidPrevEmployerdetail == 2) {
            this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Invalid service duration'});
            return;
        }

        if (IsValidPrevEmployerdetail == 3) {
           
            this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Please complete Employer Reference details'});
            return;
        }

        if (!this.ValidateProfReference()) {
            
            this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Please complete Professional Reference details'});
            return;
        }

        var profReference = this._Associate.profReference;
        for (var count = 0; count < profReference.length; count++) {
            if (!this.validateEmail(profReference[count].officeEmailAddress)) {
                
                this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Please enter valid email address. For ex:- abc@xyz.com'});
                return;
            }
            if(profReference[count].mobileNo.length < 10){
               
                this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Please enter valid contact number.'});
                return;
            }
        }

        var prevEmployerdetails = this._Associate.prevEmployerdetails;
        for (var count = 0; count < prevEmployerdetails.length; count++) {
            if (!this.IsValidDate(prevEmployerdetails[count].fromYear, today)) {
               
                this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'From date should not be greater than today date.'});
                return;
            }
            if (!this.IsValidDate(prevEmployerdetails[count].toYear , today)) {
               
                this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'To date should not be greater than or equal to today date.'});
                return;
            }           
        }


        this._Associate.empID = this.currentempID;
        this._service.SaveEmploymentDetails(this._Associate).subscribe((data) => {
           
            this.messageService.add({severity:'success', summary: 'Success Message', detail:'Employment details saved successfully.'});
            this.getEmploymentdetails(this.currentempID);
        }, (error) => {
            this.messageService.add({severity:'error', summary: 'Error Message', detail:'Failed to save employment details!'});
        });
    }

    onlyForNumbers(event: any){
        var keys = {
            'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 
            '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
        };
        for (var index in keys) {
            if (!keys.hasOwnProperty(index)) continue;
            if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                return; //default event
            }
        }
        event.preventDefault();
    };

    validateEmail(email: any) {
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(email)) {
            return true;
        }
        else {
            return false;
        }
    }

    onlyStrings(event: any)
    {   
       let k: any;  
       k = event.charCode;  //         k = event.keyCode;  (Both can be used)
       return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || k == 44 || k == 46);
    }

}



