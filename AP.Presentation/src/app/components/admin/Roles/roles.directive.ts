import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-roles',    
    template: '<app-roles-form></app-roles-form><app-roles-table></app-roles-table>',
     
})
export class RolesDirective {
}