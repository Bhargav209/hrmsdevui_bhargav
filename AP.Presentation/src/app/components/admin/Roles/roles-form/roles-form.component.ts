﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/components/common/api';
import { RoleData, RoleSufixPrefix } from '../../../../models/role.model';
import { RoleService } from '../../services/role.service';
import { GenericType } from '../../../../models/dropdowntype.model';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-roles-form',
  templateUrl: './roles-form.component.html',
  styleUrls: ['./roles-form.component.css'],
  providers: [MessageService]
})
export class RolesFormComponent implements OnInit {

  addRole : FormGroup;
   editDisable  = false;
   departmentList : SelectItem[] = [];
   roleList: SelectItem[] = [];
   prefixList: SelectItem[] = [];
   suffixList: SelectItem[] = [];
   kraList: SelectItem[] = [];
   roleMasterdata = new RoleData();
   financialYearId ;
   financialYear ;
   deptId;
   formSubmitted = false;
   disableDepartment : boolean = false;

  constructor(private rolesServiceObj : RoleService, private route : Router, private messageService: MessageService) { }

  ngOnInit() {
    this.addRole = new FormGroup({
      DepartmentId : new FormControl(null, [Validators.required] ),
      PrefixID : new FormControl(null, ),
      SGRoleID : new FormControl(null,[Validators.required] ),
      SuffixID : new FormControl(null, ),
      KRARole: new FormControl(null, ),
      DepartmentCode: new FormControl(null, ),
      KRAName : new FormControl(null, ),
      KraRole : new FormControl(null, ),
      RoleName : new FormControl(null, ),
      RoleDescription : new FormControl(null, [Validators.maxLength(4000)] ),
      EducationQualification : new FormControl(null,[Validators.maxLength(500)] ),
      KeyResponsibilities : new FormControl(null,[Validators.maxLength(500)] ),
      RoleMasterId : new FormControl(null, )
    });

    this.rolesServiceObj.roleData.subscribe(data => {
      if (this.rolesServiceObj.editMode == true) {
        this.addRole.patchValue(data);
        this.editDisable = true;
        this.editRoles(this.addRole.value.RoleMasterId);
      }
    });

    this.rolesServiceObj.selectedDepartment.subscribe(data => {
        this.deptId  = data;
        this.addRole.value.DepartmentId = this.deptId;
    });
    this.getDepartmentList();
    this.getCurrentFinancialYear();
  }

   getDepartmentList() : void{
    this.rolesServiceObj.getDepartmentList().subscribe((res : any)=>{  
     
      res.forEach(element => {
        this.departmentList.push({ label: element.Description, value: element.DepartmentId });
    });
    if(this.deptId > 0){
       this.addRole.controls['DepartmentId'].setValue(this.deptId);
       this.disableDepartment = true;
       this.getRoleSuffixAndPrefixlist(this.deptId);
    }
    });
  }
   getKRAList(event) : void{
    this.getRoleSuffixAndPrefixlist(event.value);
    this.rolesServiceObj.getKRAList(event.value).subscribe((res :  any)=>{
      res.forEach(element => {
        this.kraList.push({ label: element.Description, value: element.DepartmentId });
    });
    });

  }

 
   getRoleSuffixAndPrefixlist(DepartmentId: number): void {
    this.rolesServiceObj.GetRoleSuffixAndPrefix(DepartmentId).subscribe(
      (roleSufixPrefixResponse: RoleSufixPrefix) => {
        this.roleMasterdata.SGRoleID = null;
        this.roleMasterdata.PrefixID = null;
        this.roleMasterdata.SuffixID = null;
        this.roleList = [];
        this.roleList.push({ label: "Select a Role", value: null });
        roleSufixPrefixResponse.Roles.forEach((element: GenericType) => {
          this.roleList.push({ label: element.Name, value: element.Id });
        });
        this.prefixList = [];
        this.prefixList.push({ label: "Select a Prefix", value: null });
        roleSufixPrefixResponse.Prefix.forEach((element: GenericType) => {
          this.prefixList.push({ label: element.Name, value: element.Id });
        });
        this.suffixList = [];
        this.suffixList.push({ label: "Select a Suffix", value: null });
        roleSufixPrefixResponse.Suffix.forEach((element: GenericType) => {
          this.suffixList.push({ label: element.Name, value: element.Id });
        });
      },
  
    );
  }

   editRoles(selectedRoleId: number): void {
    this.rolesServiceObj
     .GetRoleDetailsbyRoleID(selectedRoleId)
     .subscribe((selectedRole: RoleData) => {
    //   if (this._selectedDepartmentId == 0)
    //   this.getKRAList(this._selectedDeptId);
    // else this.getKRAList(this._selectedDepartmentId);
    // this.roleMasterdata = selectedRole;
      
  });
  }



  public getCurrentFinancialYear(): void {
    this.rolesServiceObj.getCurrentFinancialYear().subscribe(
      (yearsdata: GenericType) => {
        if (yearsdata != null) {
          this.financialYearId = yearsdata.Id;
          this.financialYear = yearsdata.Name;
        }
      })
  }

   onAddRole() : void {
    this.formSubmitted = true;
    if(this.addRole.valid == true){
      if (this.addRole.value.EducationQualification == null)
      this.addRole.value.EducationQualification = "";
    if (this.addRole.value.KeyResponsibilities == null)
    this.addRole.value.KeyResponsibilities = "";
      if(this.rolesServiceObj.editMode == false){
        this.rolesServiceObj.createRole(this.addRole.value).subscribe((res)=>{
          if(res ==1){
            this.messageService.add({severity:'success', summary: 'Success Message', detail:'Role added'});
            this.resetForm();
            setTimeout(() => 
                  {
                    this.route.navigate(['/admin/rolelist/']);
                  }, 1000);
            // this.route.navigate(['/admin/rolelist/']);
          }
          else if(res == 2627)
          {
            this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'This Role already exist'});
          }
          else{
            this.messageService.add({severity:'error', summary: 'Failure Message', detail:'Role cannot be added'});
          }
        },
        error=>{
          this.messageService.add({severity:'error', summary: 'Failure Message', detail: error.error});
     
         });
      }
      else{
        this.editDisable = false;
        this.rolesServiceObj.editRole(this.addRole.value).subscribe((res)=>{
          if(res == 1){
            this.messageService.add({severity:'success', summary: 'Success Message', detail:'Role updated'});
            this.resetForm();
            setTimeout(() => 
                  {
                    this.route.navigate(['/admin/rolelist/']);
                  }, 1000);
          }
          else{
            this.messageService.add({severity:'error', summary: 'failed Message', detail:'Role cannot be updated'});
          }
        },
        error=>{
          this.messageService.add({severity:'error', summary: 'Failure Message', detail: error.error});
     
         }
      );
        this.rolesServiceObj.editMode = false;
      }
      
  }
  else{
    // this.messageService.add({severity:'warn', summary: 'Warning Message', detail:'Invalid data'});
    // this.resetForm();
    // this.route.navigate(['/admin/rolelist/']);
  }
  
}


   resetForm(): void {
    this.addRole.reset();
    this.addRole.value.Role = null;
    this.formSubmitted = false;
  }

   backToRoleList(): void {
    this.editDisable = false;
    this.rolesServiceObj.editMode = false;
    this.route.navigate(['/admin/rolelist/']);
  }
}

