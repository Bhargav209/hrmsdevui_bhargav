import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-skill-group',    
    template: '<app-skill-group-form></app-skill-group-form><app-skill-group-table></app-skill-group-table>',
     
})
export class SkillGroupDirective {
}