import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-notification-types',    
    template: '<app-notification-types-form></app-notification-types-form><app-notification-types-table></app-notification-types-table>',
     
})
export class NotificationTypesDirective {
  
}