import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-client-billing-roles',    
    template: '<app-client-billing-roles-form></app-client-billing-roles-form><app-client-billing-roles-table></app-client-billing-roles-table>',   
})

export class ClientBillingRolesDirective{

}
