import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-departments',    
    template: '<app-departments-form></app-departments-form><app-departments-table></app-departments-table>',
     
})
export class DepartmentsDirective {
  
}