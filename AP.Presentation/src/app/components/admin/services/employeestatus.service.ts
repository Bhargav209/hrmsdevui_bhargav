import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
//import 'rxjs/Rx';
import * as environmentInformation from '../../../../environments/environment';
import * as servicePath from '../../../service-paths';

@Injectable()
export class EmployeeStatusService {
    private _serverURL: string;
    private _resources: any;

    constructor(private httpClient: HttpClient) {
      this._serverURL = environmentInformation.environment.ServerUrl;
      this._resources = servicePath.API.EmployeeStatus;
    }

    GetAssociateNames() {
        let url = this._serverURL + this._resources.GetNames;
        return this.httpClient.get(url);
    }

    GetAssociates() {
        let url = this._serverURL + this._resources.GetUsers;
        return this.httpClient.get(url);
    }

    GetResignStatus(){
        let url=this._serverURL+ this._resources.GetResignStatus;
        return this.httpClient.get(url);
    }

    UpdateEmployeeStatus(empData: any) {
        let url = this._serverURL + this._resources.UpdateEmployeeStatus;
        return  this.httpClient.post(url, empData)
          
    }
    
    MapAssociateId(empData: any){
        let url = this._serverURL + this._resources.MapAssociateId;
        return this.httpClient.post(url,empData)
    }
}