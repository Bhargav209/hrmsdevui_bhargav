﻿export class Grade {
    GradeCode: string;
    GradeId: number;
    GradeName: string
    IsActive: boolean
}
