export class Client {
    ClientId?: number;
    ClientCode: string;
    ClientName: string;
    ClientRegisterName: string;
    IsActive? : boolean;
}