export class NotificationType {
    NotificationTypeID?: number;
    CategoryId?:number;
    NotificationType: string;
    Description: string;
    CategoryName:string;
}

export class NotificationConfiguration extends NotificationType{
    EmailFrom: string;
    EmailTo: string; //passing array properties to string 
    EmailCC: string; //passing array properties to string 
    ToEmail?:Email[]; 
    CCEmail?:Email[];
    EmailSubject : string;
    EmailContent : string;
}

export class Email{
    EmailID:string
}
