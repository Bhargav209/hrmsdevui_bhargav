export class CompetencyArea {
    CompetencyAreaId?: number;
    CompetencyAreaCode: string;
    CompetencyAreaDescription: string;
    IsActive?: boolean; 
  }