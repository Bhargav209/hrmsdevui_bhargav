
export class ProficiencyLevel {
    ProficiencyLevelId?: number;
    ProficiencyLevelCode: string;
    ProficiencyLevelDescription: string;
    IsActive?: boolean;
  }