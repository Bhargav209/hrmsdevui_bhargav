

export class Designation {
    DesignationId:number;
    DesignationCode:string;
    DesignationName:string;
    IsActive:boolean;
 
}

// designation page uses all designation, grade, AppreciateDropDownType classes fields
  
export class DesignationData {
    DesignationId:number;
    DesignationCode:string;
    DesignationName:string;
    IsActive:boolean;
    ID?:number;
    Name?:string;
    GradeCode: string;
    GradeId: number;
    GradeName: string
    }


