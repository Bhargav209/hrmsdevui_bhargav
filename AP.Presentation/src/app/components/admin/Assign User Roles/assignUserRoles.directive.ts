import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-assign-user-roles',    
    template: '<app-assign-user-roles-form></app-assign-user-roles-form> <app-assign-user-roles-table></app-assign-user-roles-table>',
     
})

export class AssignUserRolesDirective{

}
