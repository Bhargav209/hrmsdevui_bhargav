import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-skills',    
    template: '<app-skills-form></app-skills-form><app-skills-table></app-skills-table>',
})
export class SkillsDirective {
}