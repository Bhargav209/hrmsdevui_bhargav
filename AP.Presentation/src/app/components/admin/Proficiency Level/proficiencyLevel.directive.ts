import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-proficiency-level',    
    template: '<div class= "content-wrapper"> <app-proficiency-level-form></app-proficiency-level-form><app-proficiency-level-table></app-proficiency-level-table> </div>',
     
})
export class ProficiencyLevelDirective {
  
}