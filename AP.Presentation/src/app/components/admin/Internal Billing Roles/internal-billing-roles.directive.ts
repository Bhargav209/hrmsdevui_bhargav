import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-internal-billing-roles',    
    template: '<app-internal-billing-roles-form></app-internal-billing-roles-form> <app-internal-billing-roles-table> </app-internal-billing-roles-table>',
     
})
export class InternalBillingRolesDirective {
  
}