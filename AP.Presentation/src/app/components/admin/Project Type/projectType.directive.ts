import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-project-type',    
    template: '<div class= "content-wrapper"> <app-project-type-form></app-project-type-form><app-project-type-table></app-project-type-table> </div>',
     
})
export class ProjectTypeDirective {
}