import { Observable } from 'rxjs/Observable';
import { KRAScaleMaster, KRAScaleDetails } from '../models/krascaleData.model';
export interface IKRAScaleMaster {
    GetKRAScale(): Observable<Array<KRAScaleMaster>>;
    GetKRADescriptionDetails(kraScaleMasterId:number):Observable<Array<KRAScaleDetails>>;
    CreateKRAScale(kraScaleMaster: KRAScaleMaster): Observable<number>;
    UpdateKRAScale(kraScaleDetails: KRAScaleDetails[], maxScale:number): Observable<number>;
    DeleteKRAScale(kraScaleMasterId: number): Observable<number>;
}