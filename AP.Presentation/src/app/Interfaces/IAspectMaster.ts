import { Observable } from 'rxjs/Observable';
import { AspectData } from '../models/kra.model';
export interface IAspectMaster {
   GetAspectMasterList(): Observable<AspectData[]>;
   CreateAspect(aspectData: AspectData): Observable<number>;
   UpdateAspect(aspectData: AspectData): Observable<number>;
   DeleteAspect(aspectId: number): Observable<number>;
}